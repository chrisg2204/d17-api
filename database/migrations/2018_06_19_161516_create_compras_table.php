<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateComprasTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('compras', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('id_tipos_articulos')->unsigned();
            $table->foreign('id_tipos_articulos')
                ->references('id')->on('tipos_articulos')
                    ->onUpdate('cascade')
                    ->onDelete('cascade');
            $table->string('uuid', 36);
            $table->string('nombre_articulo', 255);
            $table->integer('cantidad');
            $table->boolean('status')->default(0);
            $table->float('costo', 8, 2);
            $table->timestampTz('fecha')->useCurrent();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('compras');
    }
}

<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCtaCteDetalleTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('cta_cte_detalle', function (Blueprint $table) {
            $table->increments('id');
            $table->string('uuid', 36);
            $table->text('items_detalle');
            $table->float('total', 8, 2);
            $table->string('n_factura', 50);
            $table->timestampTz('fecha_movimiento')->useCurrent();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('cta_cte_detalle');
    }
}

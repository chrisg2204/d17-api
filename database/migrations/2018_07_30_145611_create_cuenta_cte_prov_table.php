<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCuentaCteProvTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('cuenta_cte_prov', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('id_proveedor')->unsigned();
            $table->foreign('id_proveedor')
                ->references('id')->on('proveedores')
                    ->onUpdate('cascade')
                    ->onDelete('cascade');
            $table->integer('id_cta_cte_prov')->unsigned();
            $table->foreign('id_cta_cte_prov')
                ->references('id')->on('prov_cuenta_cte')
                    ->onUpdate('cascade')
                    ->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('cuenta_cte_prov');
    }
}

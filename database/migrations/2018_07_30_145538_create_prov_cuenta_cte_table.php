<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateProvCuentaCteTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('prov_cuenta_cte', function (Blueprint $table) {
            $table->increments('id');
            $table->string('uuid', 36);
            $table->string('descripcion_giro_propio', 255);
            $table->float('deuda_propia', 8, 2)->nullable();
            $table->float('monto_giro_propio', 8, 2)->nullable();
            $table->float('saldo_propio', 8, 2);
            $table->timestampTz('fecha_movimiento')->useCurrent();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('prov_cuenta_cte');
    }
}

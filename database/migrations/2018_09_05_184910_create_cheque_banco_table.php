<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateChequeBancoTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('cheque_banco', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('cheque_id')->unsigned();
            $table->foreign('cheque_id')
                ->references('id')->on('cheques')
                    ->onUpdate('cascade')
                    ->onDelete('cascade');
            $table->integer('banco_id')->unsigned();
            $table->foreign('banco_id')
                ->references('id')->on('bancos')
                    ->onUpdate('cascade')
                    ->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('cheque_banco');
    }
}

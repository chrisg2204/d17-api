<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePreciosMonedasTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('precios_monedas', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('id_precio')->unsigned();
            $table->foreign('id_precio')
                ->references('id')->on('precios')
                    ->onUpdate('cascade')
                    ->onDelete('cascade');
            $table->integer('id_moneda')->unsigned();
            $table->foreign('id_moneda')
                ->references('id')->on('monedas')
                    ->onUpdate('cascade')
                    ->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('precios_monedas');
    }
}

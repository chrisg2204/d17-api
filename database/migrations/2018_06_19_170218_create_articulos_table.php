<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateArticulosTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('articulos', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('id_tipos_articulos')->unsigned();
            $table->foreign('id_tipos_articulos')
                ->references('id')->on('tipos_articulos')
                    ->onUpdate('cascade')
                    ->onDelete('cascade');
            $table->string('nombre', 255);
            $table->integer('cantidad');
            $table->float('costo', 8, 2);
            $table->float('costo_usd', 8, 2)->nullable();
            $table->string('sello', 100)->nullable();
            $table->text('descripcion')->nullable();
            $table->string('foto_articulo')->nullable();
            $table->string('color', 50);
            $table->string('barcode', 100);
            $table->string('calidad', 50);
            $table->string('origen', 50)->nullable();
            $table->timestampTz('fecha_creacion')->useCurrent();
        });
    }
    
    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('articulos');
    }
}

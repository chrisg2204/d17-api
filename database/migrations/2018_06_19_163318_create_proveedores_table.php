<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateProveedoresTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('proveedores', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('id_tipo_documento')->unsigned();
            $table->foreign('id_tipo_documento')
                ->references('id')->on('tipos_documentos')
                    ->onUpdate('cascade')
                    ->onDelete('cascade');
            $table->string('n_documento', 50);
            $table->string('nombres', 100);
            $table->string('email', 100)->unique();
            $table->string('email_opt', 100)->unique()->nullable();
            $table->string('codigo_postal', 50)->nullable();
            $table->string('direccion', 255);
            $table->string('telefono', 20);
            $table->string('telefono_opt', 20)->nullable();
            $table->boolean('estatus', 1);
            $table->text('observacion');
            $table->timestampTz('fecha_creacion')->useCurrent();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('proveedores');
    }
}

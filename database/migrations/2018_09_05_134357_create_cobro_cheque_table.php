<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCobroChequeTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('cobro_cheque', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('id_cobro')->unsigned();
            $table->foreign('id_cobro')
                ->references('id')->on('cobros')
                    ->onUpdate('cascade')
                    ->onDelete('cascade');
            $table->integer('id_cheque')->unsigned();
            $table->foreign('id_cheque')
                ->references('id')->on('cheques')
                    ->onUpdate('cascade')
                    ->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('cobro_cheque');
    }
}

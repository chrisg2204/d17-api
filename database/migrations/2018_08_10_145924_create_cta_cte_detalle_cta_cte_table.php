<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCtaCteDetalleCtaCteTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('cta_cte_detalle_cta_cte', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('id_cta_cte')->unsigned();
            $table->foreign('id_cta_cte')
                ->references('id')->on('cuenta_cte')
                    ->onUpdate('cascade')
                    ->onDelete('cascade');
            $table->integer('id_detalle_cte_cta')->unsigned();
            $table->foreign('id_detalle_cte_cta')
                ->references('id')->on('cta_cte_detalle')
                    ->onUpdate('cascade')
                    ->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('cta_cte_detalle_cta_cte');
    }
}

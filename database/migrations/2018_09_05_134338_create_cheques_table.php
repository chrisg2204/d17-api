<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateChequesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('cheques', function (Blueprint $table) {
            $table->increments('id');
            $table->string('uuid', 36);
            $table->string('numero');
            $table->float('monto', 8, 2);
            $table->string('propietario', 150);
            $table->string('titular', 150)->nullable();
            $table->timestampTz('fecha_emision')->nullable();
            $table->timestampTz('fecha_cobro')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('cheques');
    }
}

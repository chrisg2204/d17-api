<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCuentaCteProvCuentaCteProvDetTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('cuenta_cte_prov_cuenta_cte_prov_det', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('id_cta_cte_prov')->unsigned();
            $table->foreign('id_cta_cte_prov')
                ->references('id')->on('cuenta_cte_prov')
                    ->onUpdate('cascade')
                    ->onDelete('cascade');
            $table->integer('id_cta_cte_prov_det')->unsigned();
            $table->foreign('id_cta_cte_prov_det')
                ->references('id')->on('cuenta_cte_prov_det')
                    ->onUpdate('cascade')
                    ->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('cuenta_cte_prov_cuenta_cte_prov_det');
    }
}

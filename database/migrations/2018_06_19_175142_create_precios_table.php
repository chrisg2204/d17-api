<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePreciosTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('precios', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('id_articulo')->unsigned();
            $table->foreign('id_articulo')
                ->references('id')->on('articulos')
                    ->onUpdate('cascade')
                    ->onDelete('cascade');
            $table->float('precio_mayor', 8, 2);
            $table->float('precio_menor', 8, 2);
            $table->float('distribuidor', 8, 2)->nullable();
            $table->float('especial', 8, 2)->nullable();
            $table->timestampTz('fecha_creacion')->useCurrent();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('precios');
    }
}

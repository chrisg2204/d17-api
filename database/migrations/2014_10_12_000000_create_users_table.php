<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateUsersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('users', function (Blueprint $table) {
            $table->increments('id');
            $table->string('email', 100)->unique();
            $table->string('nombres', 255)->nullable(false);
            $table->string('apellido', 100)->nullable(false);
            $table->boolean('status')->default(1);
            $table->text('credentials')->nullable();
            $table->string('password')->nullable(false);
            $table->string('client', 10)->nullable(true);
            $table->rememberToken();
            $table->timestampTz('fecha_creacion')->useCurrent();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('users');
    }
}

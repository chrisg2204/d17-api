<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateArticulosProveedorTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('articulos_proveedor', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('id_articulo')->unsigned();
            $table->foreign('id_articulo')
                ->references('id')->on('articulos')
                    ->onUpdate('cascade')
                    ->onDelete('cascade');
            $table->integer('id_proveedor')->unsigned();
            $table->foreign('id_proveedor')
                ->references('id')->on('articulos')
                    ->onUpdate('cascade')
                    ->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('articulos_proveedor');
    }
}

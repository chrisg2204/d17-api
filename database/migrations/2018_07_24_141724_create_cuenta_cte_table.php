<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCuentaCteTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('cuenta_cte', function (Blueprint $table) {
            $table->increments('id');
            $table->string('descripcion_giro', 255);
            $table->float('deuda_cliente', 8, 2)->nullable();
            $table->float('monto_giro', 8, 2)->nullable();
            $table->float('saldo', 8, 2)->nullable();
            $table->timestampTz('fecha_movimiento')->useCurrent();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('cuenta_cte');
    }
}

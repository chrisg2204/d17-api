<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class CtaCteDetalleCtaCte extends Model
{
	protected $fillable = [
		"id_cta_cte",
		"id_detalle_cte_cta"
	];

	protected $guarded = [
		"id"
	];

	protected $table = "cta_cte_detalle_cta_cte";

	public $timestamps = false;
}
<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class FacturaCliente extends Model
{
	protected $fillable = [
		"id_factura",
		"id_client"
	];

	protected $guarded = [
		"id"
	];

	protected $table = "factura_cliente";

	public $timestamps = false;
}

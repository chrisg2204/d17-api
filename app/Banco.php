<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Banco extends Model
{
	protected $fillable = [
		"codigo",
		"nombre"
	];

	protected $guarded = [
		"id"
	];

	protected $table = 'bancos';

	public $timestamps = false;
}

<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Cobros extends Model
{
	protected $fillable = [
		"id_pay_user",
		"type_payment",
		"type_sell",
		"fecha_creacion"
	];

	protected $guarded = [
		"id"
	];

	protected $table = "cobros";

	public $timestamps = false;
}

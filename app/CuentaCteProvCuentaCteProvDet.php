<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class CuentaCteProvCuentaCteProvDet extends Model
{
	protected $fillable = [
		"id_cta_cte_prov",
		"id_cta_cte_prov_det"
	];

	protected $guarded = [
		"id"
	];

	protected $table = "cuenta_cte_prov_cuenta_cte_prov_det";

	public $timestamps = false;
}

<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Venta extends Model
{
	protected $fillable = [
		"uuid",
		"tipo_venta",
		"items_articulo",
		"total",
		"fecha_creacion"
	];

	protected $guarded = [
		"id"
	];

	protected $table = "ventas";

	public $timestamps = false;
}

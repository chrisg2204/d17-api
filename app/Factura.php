<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Factura extends Model
{
	protected $fillable = [
		"id_venta",
		"id_seller",
		"correlativo_factura",
		"status"
	];

	protected $guarded = [
		"id"
	];

	protected $table = "facturas";

	public $timestamps = false;
}

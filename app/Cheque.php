<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Cheque extends Model
{
	protected $fillable = [
		"uuid",
		"numero",
		"monto",
		"propietario",
		"titular",
		"fecha_emision",
		"fecha_cobro"
	];

	protected $guarded = [
		"id"
	];

	protected $table = "cheques";

	public $timestamps = false;
}

<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Devolucion extends Model
{
	protected $fillable = [
		'articulos',
		'id_cliente',
		'status',
		'observacion',
		'fecha_creacion',
		'tipo'
	];

	protected $fillable = [
		'id'
	];

	protected $table = 'devoluciones';

	public $timestamps = false;
}

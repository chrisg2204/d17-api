<?php

namespace App\Exports;

// Libs.
use DB;
use Maatwebsite\Excel\Concerns\FromCollection;
use Maatwebsite\Excel\Concerns\WithHeadings;
use Maatwebsite\Excel\Concerns\WithTitle;

// Models.
use App\Articulo;
use App\Precio;

class PrecioExport implements FromCollection, WithHeadings, WithTitle
{
    /**
    * @return \Illuminate\Support\Collection
    */
    public function collection()
    {
    	return Articulo::select([DB::raw('
    		precios.id AS ID,
    		CONCAT(articulos.nombre, " | ", tipos_articulos.name, " | ", articulos.color, " | ", articulos.calidad) AS ARTICULO,
    		precios.precio_mayor AS PRECIO_X_MAYOR,
    		precios.precio_menor AS PRECIO_X_MENOR
		')])
		->join('tipos_articulos', 'tipos_articulos.id', '=', 'articulos.id_tipos_articulos')
		->join('precios', 'precios.id_articulo', '=', 'articulos.id')
		->orderBy('precios.id', 'ASC')
		->get();
    }

    public function headings(): array
    {
    	return [
    		'ID',
            'ARTICULO',
            'X_MAYOR',
            'X_MENOR'
        ];
    }
    public function title(): string
    {
        return 'Precios';
    }
}

<?php

namespace App\Exports;

// Libs.
use DB;
use Maatwebsite\Excel\Concerns\FromCollection;
use Maatwebsite\Excel\Concerns\WithHeadings;
use Maatwebsite\Excel\Concerns\WithTitle;

// Models.
use App\Articulo;

class ArticlesExport implements FromCollection, WithHeadings, WithTitle
{
    /**
    * @return \Illuminate\Support\Collection
    */
    public function collection()
    {
    	return Articulo::select([DB::raw("
    		articulos.id,
    		articulos.nombre,
    		articulos.barcode,
    		articulos.costo,
            articulos.id_tipos_articulos,
            articulos.color,
            articulos.calidad,
            articulos.cantidad
    	")])
		->get();;
    }

     public function headings(): array
    {
        return [
            'ID',
            'NOMBRE',
            'BARCODE',
            'COSTO',
            'TIPO_ART',
            'COLOR',
            'CALIDAD',
            'CANTIDAD'
        ];
    }

    public function title(): string
    {
        return 'Articulos';
    }
}

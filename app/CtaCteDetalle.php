<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class CtaCteDetalle extends Model
{
	protected $fillable = [
		"uuid",
		"items_detalle",
		"total",
		"n_factura",
		"fecha_movimiento"
	];

	protected $guarded = [
		"id"
	];

	protected $table = "cta_cte_detalle";

	public $timestamps = false;
}
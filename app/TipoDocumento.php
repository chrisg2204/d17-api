<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class TipoDocumento extends Model
{
	protected $fillable = [
		'descripcion',
		'fecha_creacion'
	];

	protected $guarded = [
		'id'
	];

	protected $table = 'tipos_documentos';

	public $timestamps = false;
}

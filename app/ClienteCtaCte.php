<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ClienteCtaCte extends Model
{
	protected $fillable = [
		"id_cliente",
		"id_cta_cte"
	];

	protected $guarded = [
		"id"
	];

	protected $table = "cliente_cta_cte";

	public $timestamps = false;
}

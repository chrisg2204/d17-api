<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class CobroCheque extends Model
{
	protected $fillable = [
		"id_cobro",
		"id_cheque"
	];

	protected $guarded = [
		"id"
	];

	protected $table = "cobro_cheque";

	public $timestamps = false;
}

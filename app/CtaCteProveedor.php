<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class CtaCteProveedor extends Model
{
	protected $fillable = [
		"id_proveedor",
		"id_cta_cte_prov"
	];

	protected $guarded = [
		"id"
	];

	protected $table = "cuenta_cte_prov";

	public $timestamps = false;
}

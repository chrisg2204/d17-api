<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

// Libs
use Validator;
use DB;
use Uuid;

// Models
use App\ProveedorCtaCte;
use App\CuentaCteProvDet;
use App\CuentaCteProvCuentaCteProvDet;
use App\CtaCteProveedor;
use App\Proveedor;

class ProveedoresCuentasCorrientes extends Controller
{
	private $success = 200;
	private $bad = 400;
	private $notFound = 404;
	private $notAuthorized = 401;
	private $conflic = 409;
	private $serverErr = 500;

	public function __construct()
	{
		// 
	}

	public function fillProviderCtaCte(Request $req)
	{
		$findAllProviderCtaCte = Proveedor::select([DB::raw(
			"
				proveedores.id AS ID,
				cuenta_cte_prov.id AS IDB,
				prov_cuenta_cte.id AS IDC,
				proveedores.n_documento AS NUM_DOC,
				proveedores.nombres AS NOM 
			"
		)])
		->leftJoin("cuenta_cte_prov", "cuenta_cte_prov.id_proveedor", "=", "proveedores.id")
		->leftJoin("prov_cuenta_cte", "prov_cuenta_cte.id", "=", "cuenta_cte_prov.id_cta_cte_prov")
		->orderBy("proveedores.id", "DESC")
		->get();

		return response()->json([
			"rows" => $findAllProviderCtaCte
		], $this->success);
	}

	public function getAccount(Request $req)
	{
		$providerId = $req->route("id");

		$offset = ($req->offset !== null) ? $req->offset : 0;
		$limit = ($req->limit !== null) ? $req->limit : 10;
		$searchType = ($req->searchType !== null) ? $req->searchType : "all";

		$verifyArr = ['limit' => $limit, 'offset' => $offset];

		$validator = Validator::make($verifyArr, [
			'limit' => 'numeric',
			'offset' => 'numeric'
		], [
			'limit.numeric' => 'Limit debe ser numérico.',
			'offset.numeric' => 'Offset debe ser numérico.'
		]);

		if ($validator->fails()) {
			return response()->json([
				"errors" => $validator->errors()
			], $this->bad);
		}

		$findOneProvider = Proveedor::where("id", $providerId)
									->first();
		if (!$findOneProvider) {
			return response()->json([
				"success" => false,
				"content" => "Proveedor ".$providerId." no encontrado."
			], $this->notFound);
		}

		$findAccountMovement = CtaCteProveedor::select([DB::raw(
			"
				cuenta_cte_prov.id AS AID,
				prov_cuenta_cte.id AS BID,
				prov_cuenta_cte.descripcion_giro_propio AS DES_GIR_PRO,
				prov_cuenta_cte.deuda_propia AS DEU_PRO,
				prov_cuenta_cte.monto_giro_propio AS MON_PRO,
				prov_cuenta_cte.saldo_propio AS SAL_PRO,
				DATE_FORMAT(prov_cuenta_cte.fecha_movimiento,'%d/%m/%Y') AS FEC_MOV,
				cuenta_cte_prov_det.n_factura AS N_FAC
			"
		)])
		->join("prov_cuenta_cte", "prov_cuenta_cte.id", "=", "cuenta_cte_prov.id_cta_cte_prov")
		->join("cuenta_cte_prov_cuenta_cte_prov_det", "prov_cuenta_cte.id", "=", "cuenta_cte_prov_cuenta_cte_prov_det.id_cta_cte_prov")
		->join("cuenta_cte_prov_det", "cuenta_cte_prov_cuenta_cte_prov_det.id_cta_cte_prov_det", "=", "cuenta_cte_prov_det.id")
		->where("cuenta_cte_prov.id_proveedor", $providerId)
		->orderBy("prov_cuenta_cte.id", "ASC")
		->get();

		return response()->json([
			"rows" => $findAccountMovement
		], $this->success);
	}

	public function getDetailMovement(Request $req)
	{
		$movId = $req->route("id");

		$offset = ($req->offset !== null) ? $req->offset : 0;
		$limit = ($req->limit !== null) ? $req->limit : 10;

		$searchType = ($req->searchType !== null) ? $req->searchType : "all";
		$verifyArr = ['limit' => $limit, 'offset' => $offset];

		$validator = Validator::make($verifyArr, [
			'limit' => 'numeric',
			'offset' => 'numeric'
		], [
			'limit.numeric' => 'Limit debe ser numérico.',
			'offset.numeric' => 'Offset debe ser numérico.'
		]);

		if ($validator->fails()) {
			return response()->json([
				"errors" => $validator->errors()
			], $this->bad);
		}

		$findOneMov = ProveedorCtaCte::where("id", $movId)
									->first();
		if (!$findOneMov) {
			return response()->json([
				"success" => false,
				"content" => "Movimiento ".$movId." no encontrado."
			], $this->bad);
		}

		$findDetAccount = ProveedorCtaCte::select([DB::raw("
			prov_cuenta_cte.id AS AID,
			cuenta_cte_prov_cuenta_cte_prov_det.id AS BID,
			cuenta_cte_prov_det.id AS CID,
			cuenta_cte_prov_det.uuid AS UUI,
    		cuenta_cte_prov_det.items_detalle AS ITE_DET,
    		cuenta_cte_prov_det.total AS TTL,
    		cuenta_cte_prov_det.n_factura AS N_FAC,
    		DATE_FORMAT(cuenta_cte_prov_det.fecha_movimiento,'%d/%m/%Y') AS FEC_MOV
		")])
		->join("cuenta_cte_prov_cuenta_cte_prov_det", "prov_cuenta_cte.id", "=", "cuenta_cte_prov_cuenta_cte_prov_det.id_cta_cte_prov")
		->join("cuenta_cte_prov_det", "cuenta_cte_prov_cuenta_cte_prov_det.id_cta_cte_prov_det", "=", "cuenta_cte_prov_det.id")
		->where("prov_cuenta_cte.id", "=", $movId)
		->orderBy("prov_cuenta_cte.id", "ASC")
		->get();

		return response()->json([
			"rows" => $findDetAccount
		], $this->success);
	}

	public function addAccountMovement(Request $req)
	{
		$providerId = $req->route("id");

		$validator = Validator::make($req->all(), [
			"descripcion" => "required",
			"ingreso" => "nullable|numeric",
			"pago" => "nullable|numeric",
			"saldo" => "required|numeric",
			"fecha" => "required",
			"nFactura" => "required|numeric"
		], [
			"descripcion.required" => "Descripción del Movimiento requerida.",
			"nFactura.required" => "Campos faltantes en Detalle.",
			"nFactura.numeric" => "Número de Factura debe ser numérico.",
			"ingreso.numeric" => "Ingreso debe ser numérico.",
			"pago.numeric" => "Pago debe ser numérico.",
			"saldo.required" => "Saldo requerido.",
			"saldo.numeric" => "Saldo debe ser numérico.",
			"fecha.required" => "Fecha requerida.",
		]);

		if ($validator->fails()) {
			return response()->json([
				"errors" => $validator->errors()
			], $this->bad);
		}

		$findOneProvider = Proveedor::where("id", $providerId)
									->first();

		if (!$findOneProvider) {
			return response()->josn([
				"success" => false,
				"content" => "Proveedor".$providerId." no encontrado."
			], $this->notFound);
		}

		$body = $req->all();

		$newMovement = new ProveedorCtaCte;
		$newMovement->uuid = Uuid::generate(4);
		$newMovement->descripcion_giro_propio = $body["descripcion"];
		$newMovement->deuda_propia = ($body["ingreso"] == 0) ? ($body["ingreso"] = null) : $body["ingreso"];
		$newMovement->monto_giro_propio = ($body["pago"] == 0) ? ($body["pago"] = null) : $body["pago"];
		$newMovement->saldo_propio = $body["saldo"];
		$date = str_replace('/', '-', $body["fecha"]);
		$newMovement->fecha_movimiento = date('Y-m-d H:i:s', strtotime($date));

		$movementSaved = $newMovement->save();

		if (!$movementSaved) {
			return response()->json([
				"success" => false,
				"content" => "Error al registrar giro para el proveedor ".$providerId
			], $this->serverErr);
		} else {
			$newCtaCteProv = new CtaCteProveedor;
			$newCtaCteProv->id_proveedor = $providerId;
			$newCtaCteProv->id_cta_cte_prov = $newMovement->id;

			$ctaCteProvSaved = $newCtaCteProv->save();

			if (!$ctaCteProvSaved) {
				return response()->json([
					"success" => false,
					"content" => "Error al relacionar giro ".$newMovement->id." con cliente ".$providerId
				], $this->serverErr);
			} else {
				$ctaCteProvDet = new CuentaCteProvDet;
				$ctaCteProvDet->uuid = Uuid::generate(4);
				$ctaCteProvDet->items_detalle = $body["detalle"];
				$ctaCteProvDet->total = $body["total"];
				$ctaCteProvDet->n_factura = $body["nFactura"];
				$ctaCteProvDet->fecha_movimiento = date('Y-m-d H:i:s', strtotime($date));

				$ctaCteProvDetSaved = $ctaCteProvDet->save();

				if (!$ctaCteProvDetSaved) {
					return response()->json([
						"success" => false,
						"content" => "Error al registrar detalle para el giro ".$newMovement->id
					], $this->serverErr);
				} else {
					$ctaCteProvCtaCteProvDet = new CuentaCteProvCuentaCteProvDet;
					$ctaCteProvCtaCteProvDet->id_cta_cte_prov = $newMovement->id;
					$ctaCteProvCtaCteProvDet->id_cta_cte_prov_det = $ctaCteProvDet->id;

					$ctaCteProvCtaCteProvDetSaved = $ctaCteProvCtaCteProvDet->save();

					if (!$ctaCteProvCtaCteProvDetSaved) {
						return response()->json([
							"success" => false,
							"content" => "Error al relacionar giro ".$newMovement->id." con detalle "
						], $this->serverErr);
					} else {
						return response()->json([
							"success" => true,
							"content" => "Giro registrado exitosamente."
						], $this->success);
					}
				}
			}
		}
	}

}


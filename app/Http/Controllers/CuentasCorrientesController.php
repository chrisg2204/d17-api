<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

// Libs
use Validator;
use Uuid;
use DB;

// Models
use App\CtaCteDetalleCtaCte;
use App\CtaCteDetalle;
use App\CtaCte;
use App\ClienteCtaCte;
use App\Cliente;

class CuentasCorrientesController extends Controller
{
	private $success = 200;
	private $bad = 400;
	private $notFound = 404;
	private $notAuthorized = 401;
	private $conflic = 409;
	private $serverErr = 500;

	public function __construct()
	{
		// 
	}

	public function fillClientCtaCte(Request $req)
	{
		$findAllClientCtaCte = Cliente::select([DB::raw(
			"
				clientes.id AS ID,
				cliente_cta_cte.id AS IDB,
				cuenta_cte.id AS IDC,
				clientes.n_documento AS N_DOC,
				clientes.nombres AS NOM,
    			CONCAT(clientes.nombres, ', ', substring_index(substring_index(`direccion`,',',-1),',',1)) AS NOM_ZON,
				clientes.direccion AS DIR,
				clientes.telefono AS TEL,
				clientes.observacion AS OBS
			"
		)])
		->leftjoin("cliente_cta_cte", "cliente_cta_cte.id_cliente", "=", "clientes.id")
		->leftjoin("cuenta_cte", "cuenta_cte.id", "=", "cliente_cta_cte.id_cta_cte")
		->orderBy("clientes.nombres", "ASC")
		->get();

		return response()->json([
			"rows" => $findAllClientCtaCte,
		], $this->success);

	}

	public function getAccount(Request $req)
	{
		$clientId = $req->route("id");

		$offset = ($req->offset !== null) ? $req->offset : 0;
		$limit = ($req->limit !== null) ? $req->limit : 10;
		$searchType = ($req->searchType !== null) ? $req->searchType : "all";

		$verifyArr = ['limit' => $limit, 'offset' => $offset];

		$validator = Validator::make($verifyArr, [
			'limit' => 'numeric',
			'offset' => 'numeric'
		], [
			'limit.numeric' => 'Limit debe ser numérico.',
			'offset.numeric' => 'Offset debe ser numérico.'
		]);

		if ($validator->fails()) {
			return response()->json([
				"errors" => $validator->errors()
			], $this->bad);
		}

		$findOneClient = Cliente::where("id", $clientId)
								->first();
		if (!$findOneClient) {
			return response()->json([
				"success" => false,
				"content" => "Cliente ".$clientId." no encontrado."
			], $this->notFound);
		}

		$findAccountMovement = ClienteCtaCte::select([DB::raw(
			"
				cliente_cta_cte.id AS AID,
				cuenta_cte.id AS BID,
				cuenta_cte.descripcion_giro AS DES_GIR,
				cuenta_cte.deuda_cliente AS DEU_CLI,
				cuenta_cte.monto_giro AS MON_GIR,
				cuenta_cte.saldo AS SAL,
				DATE_FORMAT(cuenta_cte.fecha_movimiento,'%d/%m/%Y') AS FEC_MOV,
				cta_cte_detalle.n_factura AS N_FAC
			"
		)])
		->join("cuenta_cte", "cliente_cta_cte.id_cta_cte", "=", "cuenta_cte.id")
		->join("cta_cte_detalle_cta_cte", "cuenta_cte.id", "=", "cta_cte_detalle_cta_cte.id_cta_cte")
		->join("cta_cte_detalle", "cta_cte_detalle_cta_cte.id_detalle_cte_cta", "=", "cta_cte_detalle.id")
		->where("cliente_cta_cte.id_cliente", $clientId)
		->orderBy("cuenta_cte.id", "ASC")
		->get();

		return response()->json([
			"rows" => $findAccountMovement
		], $this->success);
	}

	public function getDetailMovement(Request $req)
	{
		$movId = $req->route("id");
		$codeInv = $req->route("codeInv");

		$offset = ($req->offset !== null) ? $req->offset : 0;
		$limit = ($req->limit !== null) ? $req->limit : 10;

		$searchType = ($req->searchType !== null) ? $req->searchType : "all";
		$verifyArr = ['limit' => $limit, 'offset' => $offset];

		$validator = Validator::make($verifyArr, [
			'limit' => 'numeric',
			'offset' => 'numeric'
		], [
			'limit.numeric' => 'Limit debe ser numérico.',
			'offset.numeric' => 'Offset debe ser numérico.'
		]);

		if ($validator->fails()) {
			return response()->json([
				"errors" => $validator->errors()
			], $this->bad);
		}

		$findOneMov = CtaCte::where("id", $movId)
									->first();
		if (!$findOneMov) {
			return response()->json([
				"success" => false,
				"content" => "Movimiento ".$movId." no encontrado."
			], $this->bad);
		}

		$findDetAccount = CtaCte::select([DB::raw("
			cuenta_cte.id AS AID,
			cta_cte_detalle_cta_cte.id AS BID,
			cta_cte_detalle.id AS CID,
			cta_cte_detalle.uuid AS UUI,
    		cta_cte_detalle.items_detalle AS ITE_DET,
    		cta_cte_detalle.total AS TTL,
    		cta_cte_detalle.n_factura AS N_FAC,
    			(select cobros.type_payment from facturas
    			inner join cobros_factura on facturas.id = cobros_factura.id_factura
    			inner join cobros on cobros_factura.id_cobro = cobros.id
    				where facturas.correlativo_factura = '".$codeInv."') AS PAG,
    		DATE_FORMAT(cta_cte_detalle.fecha_movimiento,'%d/%m/%Y') AS FEC_MOV
		")])
		->join("cta_cte_detalle_cta_cte", "cuenta_cte.id", "=", "cta_cte_detalle_cta_cte.id_cta_cte")
		->join("cta_cte_detalle", "cta_cte_detalle_cta_cte.id_detalle_cte_cta", "=", "cta_cte_detalle.id")
		->where("cuenta_cte.id", "=", $movId)
		->orderBy("cuenta_cte.id", "ASC")
		->get();

		return response()->json([
			"rows" => $findDetAccount
		], $this->success);
	}

	public function addAccountMovement(Request $req)
	{
		$clientId = $req->route("id");

		$validator = Validator::make($req->all(), [
			"descripcion" => "required",
			"ingreso" => "nullable|numeric",
			"pago" => "nullable|numeric",
			"saldo" => "required|numeric",
			"fecha" => "required",
			"nFactura" => "required|numeric"
		], [
			"descripcion.required" => "Descripción del Movimiento requerida.",
			"nFactura.required" => "Campos faltantes en Detalle.",
			"nFactura.numeric" => "Número de Factura debe ser numérico.",
			"ingreso.numeric" => "Ingreso debe ser numérico.",
			"pago.numeric" => "Pago debe ser numérico.",
			"saldo.required" => "Saldo requerido.",
			"saldo.numeric" => "Saldo debe ser numérico.",
			"fecha.required" => "Fecha requerida."
		]);

		if ($validator->fails()) {
			return response()->json([
				"errors" => $validator->errors()
			], $this->bad);
		}

		$findOneClient = Cliente::where("id", $clientId)
								->first();
		if (!$findOneClient) {
			return response()->json([
				"success" => false,
				"content" => "Cliente ".$clientId." no encontrado."
			], $this->notFound);
		}

		$body = $req->all();

		$newMovement = new CtaCte;
		$newMovement->descripcion_giro = $body["descripcion"];
		$newMovement->deuda_cliente = ($body["ingreso"] == 0) ? ($body["ingreso"] = null) : $body["ingreso"];
		$newMovement->monto_giro = ($body["pago"] == 0) ? ($body["pago"] = null) : $body["pago"];
		$newMovement->saldo = $body["saldo"];
		$date = str_replace('/', '-', $body["fecha"]);
		$newMovement->fecha_movimiento = date('Y-m-d H:i:s', strtotime($date));

		$movementSaved = $newMovement->save();

		if (!$movementSaved) {
			return response()->json([
				"success" => false,
				"content" => "Error al registrar giro para el cliente ".$clientId
			], $this->serverErr);
		} else {
			$newClienteCtaCte = new ClienteCtaCte;
			$newClienteCtaCte->id_cliente = $clientId;
			$newClienteCtaCte->id_cta_cte = $newMovement->id;

			$clienteCtaCteSaved = $newClienteCtaCte->save();

			if (!$clienteCtaCteSaved) {
				return response()->json([
					"success" => false,
					"content" => "Error al relacionar giro ".$newMovement->id." con cliente ".$clientId
				], $this->serverErr);
			} else {
				$ctaCteDet = new CtaCteDetalle;

				$ctaCteDet->uuid = Uuid::generate(4);
				$ctaCteDet->items_detalle = $body["detalle"];
				$ctaCteDet->total = $body["total"];
				$ctaCteDet->n_factura = $body["nFactura"];
				$ctaCteDet->fecha_movimiento = date('Y-m-d H:i:s', strtotime($date));

				$ctaCteDetSaved = $ctaCteDet->save();

				if (!$ctaCteDetSaved) {
					return response()->json([
						"success" => false,
						"content" => "Error al registrar detalle para el giro ".$newMovement->id
					], $this->serverErr);
				} else {
					$ctaCteDetalleCtaCte = new CtaCteDetalleCtaCte;
					$ctaCteDetalleCtaCte->id_cta_cte = $newMovement->id;
					$ctaCteDetalleCtaCte->id_detalle_cte_cta = $ctaCteDet->id;

					$ctaCteDetalleCtaCteSaved = $ctaCteDetalleCtaCte->save();

					if (!$ctaCteDetalleCtaCteSaved) {
						return response()->json([
							"success" => false,
							"content" => "Error al relacionar giro ".$newMovement->id." con detalle"
						], $this->serverErr);
					} else {
						return response()->json([
							"success" => true,
							"content" => "Giro registrado exitosamente."
						], $this->success);
					}
				}				
			}
		}
	}
}

<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

// Models
use App\Moneda;

// Libs
use Validator;
use DB;


class MonedaController extends Controller
{
	private $success = 200;
	private $bad = 400;
	private $notFound = 404;
	private $notAuthorized = 401;
	private $conflic = 409;
	private $serverErr = 500;

	public function __construct()
	{
		//
	}

	public function addCurrency(Request $req)
	{
		$validator = Validator::make($req->all(), [
			"cambio" => "required|numeric"
		], [
			"cambio.required" => "Cambio requerido.",
			"cambio.numeric" => "Cambio invalido."
		]);

		if ($validator->fails()) {
			return response()->json([
				"errors" => $validator->errors()
			], $this->bad);
		}

		$body = $req->all();

		$newPrice = new Moneda;
		$newPrice->denominacion = 'USD';
		$newPrice->simbolo = '$';
		$newPrice->cambio = $body['cambio'];
		$newPrice->fecha_creacion = date("Y-m-d H:i:s");
		$currencySaved = $newPrice->save();

		if (!$currencySaved) {
			return response()->json([
				"success" => false,
				"content" => "Error al actualizar moneda."
			], $this->serverErr);
		} else {
			return response()->json([
				"success" => true,
				"content" => "Moneda actualizada exitosamente."
			],$this->success);
		}
	}

	public function findAllCurrency(Request $req)
	{
		$offset = ($req->offset !== null) ? $req->offset : 0;
		$limit = ($req->limit !== null) ? $req->limit : 10;
		$searchType = ($req->searchType !== null) ? $req->searchType : "all";

		$verifyArr = ['limit' => $limit, 'offset' => $offset];

		$validator = Validator::make($verifyArr, [
			'limit' => 'numeric',
			'offset' => 'numeric'
		], [
			'limit.numeric' => 'Limit debe ser numerico.',
			'offset.numeric' => 'Offset debe ser numerico.'
		]);

		if ($validator->fails()) {
			return response()->json([
				"errors" => $validator->errors()
			], $this->bad);
		}

		$findAll = Moneda::select([DB::raw('
			monedas.id AS ID,
			monedas.denominacion AS DENOMINATION,
			monedas.simbolo AS SYMBOL,
			monedas.cambio AS EXCHANGE,
			monedas.fecha_creacion AS CREATED
		')])
		->orderBy('fecha_creacion', 'DESC')
		// ->take($limit)
        // ->skip($offset)
		->get();

		return response()->json([
			"rows" => $findAll
		], $this->success);
	}

	public function fillCurrency(Request $req)
	{
		$findAll = Moneda::select([DB::raw('
			monedas.id AS ID,
			monedas.cambio AS EXCHANGE,
			CONCAT(monedas.denominacion, " ", monedas.cambio, monedas.simbolo, " -> ", DATE_FORMAT(monedas.fecha_creacion, "%d/%m/%Y")) AS DESCRIPTION
		')])
		->orderBy('id', 'desc')
		->get();

		return response()->json([
			"rows" => $findAll
		]);
	}
}

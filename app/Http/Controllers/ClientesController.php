<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

// Libs
use Validator;
use DB;

// Models
use App\Cliente;

class ClientesController extends Controller
{
	private $success = 200;
	private $bad = 400;
	private $notFound = 404;
	private $notAuthorized = 401;
	private $conflic = 409;
	private $serverErr = 500;

	public function __construct()
	{
		// 
	}

	public function addClient(Request $req)
	{
		$validator = Validator::make($req->all(), [
			"idTipoDocumento" => "required|numeric",
			"numDocumento" => "required|numeric",
			"email" => "required|email",
			"emailOpt" => "nullable|email",
			"nombres" => "required|regex:/^[\pL\s\-]+$/u",
			"direccion" => "required",
			"codigoPostal" => "nullable",
			"telefono" => "required|numeric",
			"telefonoOpt" => "nullable|numeric",
			"observacion" => "required"
		], [
			"idTipoDocumento.required" => "Tipo de documento requerido.",
			"idTipoDocumento.numeric" => "Tipo de documento inválido.",
			"numDocumento.required" => "Número de Documento requerido.",
			"numDocumento.numeric" => "Número de Documento inválido.",
			"email.required" => "Email requerido.",
			"email.email" => "Formato de email inválido.",
			"nombres.required" => "Nombre requerido.",
			"nombres.regex" => "Nombre no debe tener caracteres especiales.",
			"direccion.required" => "Dirección requerida.",
			"telefono.required" => "Teléfono requerido.",
			"telefono.numeric" => "Teléfono debe ser numerico.",
			"observacion.required" => "Observación requerida.",
		]);

		if ($validator->fails()) {
			return response()->json([
				"errors" => $validator->errors()
			], $this->bad);
		}

		$body = $req->all();

		$findOneEmail = Cliente::where("email", $body["email"])
							->first();
		$findOneNumDoc = Cliente::where("n_documento", $body["numDocumento"])
							->first();
		$findOnePhone = Cliente::where("telefono", $body["telefono"])
							->first();

		if ($findOneEmail) {
			return response()->json([
				"success" => false,
				"content" => "Email ".$body["email"]." ya existe."
			], $this->conflic);
		}
		if ($findOneNumDoc) {
			return response()->json([
				"success" => false,
				"content" => "Número de Documento ".$body["numDocumento"]." ya existe."
			], $this->conflic);
		}
		if ($findOnePhone) {
			return response()->json([
				"success" => false,
				"content" => "Teléfono ".$body["telefono"]." ya existe."
			], $this->bad);
		}
		if ($body["email"] == $body["emailOpt"]) {
			return response()->json([
				"success" => false,
				"content" => "Email principal no debe ser igual a email opcional."
			], $this->bad);
		}
		if ($body["emailOpt"] == $body["email"]) {
			return response()->json([
				"success" => false,
				"content" => "Email opcional no debe ser igual a email principal."
			], $this->bad);
		}
		if ($body["telefono"] == $body["telefonoOpt"]) {
			return response()->json([
				"success" => false,
				"content" => "Teléfono principal no debe ser igual a el opcional."
			],$this->bad);
		}
		if ($body["telefonoOpt"] == $body["telefono"]) {
			return response()->json([
				"success" => false,
				"content" => "Teléfono opcional no debe ser igual a el Teléfono principal."
			], $this->bad);
		}
		if ($body["idTipoDocumento"] == 1 || $body["idTipoDocumento"] == 2) {
			if (strlen($body["numDocumento"]) < 11) {
				return response()->json([
					"success" => false,
					"content" => "Número de Documento ".$body["numDocumento"]." invalido."
				], $this->bad);	
			}
		} elseif ($body["idTipoDocumento"] == 3) {
			if (strlen($body["numDocumento"]) <> 8) {
				return response()->json([
					"success" => false,
					"content" => "Número de Documento ".$body["numDocumento"]." invalido." 
				], $this->bad);
			}
		}

		$newClient = new Cliente;
		$newClient->id_tipo_documento = $body["idTipoDocumento"];
		$newClient->n_documento = $body["numDocumento"];
		$newClient->email = $body["email"];
		if (array_key_exists("emailOpt", $body)) {
			$newClient->email_opt = $body["emailOpt"];
		}
		$newClient->nombres = $body["nombres"];
		$newClient->direccion = $body["direccion"];
		if (array_key_exists("codigoPostal", $body)) {
			$newClient->codigo_postal = $body["codigoPostal"];
		}
		$newClient->telefono = $body["telefono"];
		if (array_key_exists("telefonoOpt", $body)) {
			$newClient->telefono_opt = $body["telefonoOpt"];
		}
		$newClient->observacion = $body["observacion"];
		$newClient->estatus = 1;

		$clientSaved = $newClient->save();

		if (!$clientSaved) {
			return response()->json([
				"success" => false,
				"content" => "Error al registrar cliente."
			], $this->serverErr);
		} else {
			return response()->json([
				"success" => true,
				"content" => "Cliente registrado exitosamente."
			], $this->success);
		}
	}

	public function findAllClients(Request $req)
	{
		$offset = ($req->offset !== null) ? $req->offset : 0;
		$limit = ($req->limit !== null) ? $req->limit : 10;
		$searchType = ($req->searchType !== null) ? $req->searchType : "all";

		$verifyArr = ['limit' => $limit, 'offset' => $offset];

		$validator = Validator::make($verifyArr, [
			'limit' => 'numeric',
			'offset' => 'numeric'
		], [
			'limit.numeric' => 'Limit debe ser numerico.',
			'offset.numeric' => 'Offset debe ser numerico.'
		]);

		if ($validator->fails()) {
			return response()->json([
				"errors" => $validator->errors()
			], $this->bad);
		}

		$allClients = Cliente::select([DB::raw('
			clientes.id AS ID,
			clientes.id_tipo_documento AS ID_TIP_DOC,
			tipos_documentos.descripcion AS TIP_DOC_DES,
			clientes.n_documento AS N_DOC,
			clientes.email AS EMAIL,
			clientes.email_opt AS EMAIL_OPT,
			clientes.nombres AS NOM,
			clientes.direccion AS DIR,
			clientes.codigo_postal AS COD_POS,
			clientes.telefono AS TEL,
			clientes.telefono_opt AS TEL_OPT,
			clientes.observacion AS OBS,
			clientes.estatus AS EST,
			clientes.fecha_creacion AS FEC_CRE
		')])
		->leftjoin('tipos_documentos', 'tipos_documentos.id', '=', 'clientes.id_tipo_documento')
		->where('estatus', 1)
		->orderBy('clientes.id', 'DESC')
		->get();

		return response()->json([
			"rows" => $allClients
		], $this->success);
	}

	public function editClient(Request $req)
	{
		$idClient = $req->route('id');

		$validator = Validator::make($req->all(), [
			"idTipoDocumento" => "nullable|numeric",
			"numDocumento" => "nullable",
			"email" => "nullable|email",
			"nombres" => "nullable|regex:/^[\pL\s\-]+$/u",
			"direccion" => "nullable",
			"codigoPostal" => "nullable",
			"telefono" => "nullable",
			"observacion" => "nullable",
			"estatus" => "nullable|numeric"
		], [
			"idTipoDocumento.numeric" => "Tipo de documento inválido.",
			"nombres.regex" => "Nombre no debe tener caracteres especiales",
			"email.email" => "Formato de email inválido.",
			"estatus.numeric" => "Estatus inválido"
		]);

		if ($validator->fails()) {
			return response()->json([
				"errors" => $validator->errors()
			], $this->bad);
		}

		$body = $req->all();
		$findOneClient = Cliente::where("id", $idClient)
							->first();

		if (!$findOneClient) {
			return response()->json([
				"success" => true,
				"content" => "Cliente ".$idClient." no encontrado."
			], $this->notFound);
		} else {
			if (array_key_exists("idTipoDocumento", $body)) {
				$findOneClient->id_tipo_documento = $body["idTipoDocumento"];
			}
			if (array_key_exists("numDocumento", $body)) {
				$findOneClient->n_documento = $body["numDocumento"];
			}
			if (array_key_exists("email", $body)) {
				$findOneClient->email = $body["email"];
			}
			if (array_key_exists("nombres", $body)) {
				$findOneClient->nombres = $body["nombres"];
			}
			if (array_key_exists("direccion", $body)) {
				$findOneClient->direccion = $body["direccion"];
			}
			if (array_key_exists("codigoPostal", $body)) {
				$findOneClient->codigo_postal = $body["codigoPostal"];
			}
			if (array_key_exists("telefono", $body)) {
				$findOneClient->telefono = $body["telefono"];
			}
			if (array_key_exists("observacion", $body)) {
				$findOneClient->observacion = $body["observacion"];
			}
			if (array_key_exists("estatus", $body)) {
				$findOneClient->estatus = $body["estatus"];
			}

			$findOneClientSaved = $findOneClient->save();

			if (!$findOneClientSaved) {
				return response()->json([
					"success" => false,
					"content" => "Error al actualizar datos del cliente"
				],$this->serverErr);
			} else {
				return response()->json([
					"success" => true,
					"content" => "Datos del cliente actualizados exitosamente."
				], $this->success);
			}
		}
	}

	public function softDeleteClient(Request $req)
	{
		$idClient = $req->route('id');

		$findOneClient = Cliente::where('id', $idClient)
							->first();

		if (!$findOneClient) {
			return response()->json([
				"success" => false,
				"content" => "Cliente ".$idClient." no encontrado."
			], $this->notFound);
		} else {
			$findOneClient->estatus = 0;
			$findOneClientSaved = $findOneClient->save();

			if (!$findOneClientSaved) {
				return response()->json([
					"success" => false,
					"content" => "Error al deshabiliar cliente ".$idClient
				], $this->serverErr);
			} else {
				return response()->json([
					"success" => true,
					"content" => "Cliente deshabilitado exitosamente."
				], $this->success);
			}
		}
	}

	public function getDisabledClient(Request $req)
	{
		$offset = ($req->offset !== null) ? $req->offset : 0;
		$limit = ($req->limit !== null) ? $req->limit : 10;
		$searchType = ($req->searchType !== null) ? $req->searchType : "all";

		$verifyArr = ['limit' => $limit, 'offset' => $offset];

		$validator = Validator::make($verifyArr, [
			'limit' => 'numeric',
			'offset' => 'numeric'
		], [
			'limit.numeric' => 'Limit debe ser numerico.',
			'offset.numeric' => 'Offset debe ser numerico.'
		]);

		if ($validator->fails()) {
			return response()->json([
				"errors" => $validator->errors()
			], $this->bad);
		}

		$findAllDisabledClients = Cliente::select([DB::raw('
			clientes.id AS ID,
			clientes.email AS EMAIL,
			clientes.nombres AS NOM,
			clientes.estatus AS EST,
			clientes.telefono AS TEL,
			clientes.fecha_creacion AS FEC_CRE
		')])
		->where('estatus', 0)
		->orderBy('clientes.id', 'DESC')
		->get();

		return response()->json([
			"rows" => $findAllDisabledClients
		], $this->success);
	}

	public function enableClient(Request $req)
	{
		$idClient = $req->route('id');

		$findOneClient = Cliente::where('id', $idClient)
							->first();

		if (!$findOneClient) {
			return response()->json([
				"success" => false,
				"content" => "Cliente ".$idClient." no encontrado."
			], $this->notFound);
		}

		$findOneClient->estatus = 1;

		$clientSaved = $findOneClient->save();

		if (!$clientSaved) {
			return response()->json([
				"success" => false,
				"content" => "Error al habilidar cliente."
			], $this->serverErr);
		} else {
			return response()->json([
				"success" => true,
				"content" => "Cliente habilitado exitosamente."
			], $this->success);
		}
	}

}

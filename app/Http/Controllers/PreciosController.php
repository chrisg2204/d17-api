<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

// Models
use App\Articulo;
use App\TipoArticulo;
use App\Precio;
use App\PrecioMoneda;
use App\Moneda;

// Libs
use Validator;
use DB;

class PreciosController extends Controller
{
	private $success = 200;
	private $bad = 400;
	private $notFound = 404;
	private $notAuthorized = 401;
	private $conflic = 409;
	private $serverErr = 500;

	public function __construct()
	{
		//
	}

	public function addPrices(Request $req)
	{
		$idArticle = $req->route('id');

		$validator = Validator::make($req->all(), [
			"idMoneda" => "required",
			"precioMayor" => "required|numeric",
			"precioMenor" => "required|numeric",
			"distribuidor" => "sometimes|numeric",
			"especial" => "sometimes|numeric"
		], [
			"idMoneda.required" => "Moneda requerida.", 
			"precioMayor.required" => "Precio por Mayor requerido.",
			"precioMayor.numeric" => "Precio por Mayor invalido.",
			"precioMenor.required" => "Precio por Menor requerido.",
			"precioMenor.numeric" => "Precio por Menor invalido.",
			"distribuidor.numeric" => "Precio Distribuidor invalido.",
			"especial.numeric" => "Precio especial invalido."
		]);

		if ($validator->fails()) {
			return response()->json([
				"errors" => $validator->errors()
			], $this->bad);
		}

		$body = $req->all();

		$findArticle = Articulo::where('id', $idArticle)
			->first();

		if (!$findArticle) {
			return response()->json([
				"success" => false,
				"content" => "Articulo ".$idArticle." no encontrado."
			], $this->notFound);
		} else {
			$findPrecio = Precio::where('id_articulo', $idArticle)
				->first();

				if (!$findPrecio) {
					// if ($body['precioMayor'] < $findArticle->costo) {
					// 	return response()->json([
					// 		"success" => false,
					// 		"content" => "Precio mayor no debe ser menor a costo."
					// 	], $this->bad);
					// }
					if ($body['precioMayor'] > $body['precioMenor']) {
						return response()->json([
							"success" => false,
							"content" => "Precio por Mayor no debe ser mayor a precio menor."
						], $this->bad);
					}
					// if ($body['precioMenor'] < $findArticle->costo) {
					// 	return response()->json([
					// 		"success" => false,
					// 		"content" => "Precio menor no debe ser menor a costo."
					// 	], $this->bad);
					// }
					if ($body['precioMenor'] < $body['precioMayor']) {
						return response()->json([
							"success" => false,
							"content" => "Precio por Menor no debe ser menor a precio mayor."
						], $this->bad);
					}

					$newPrecio = new Precio;
					$newPrecio->id_articulo = $idArticle;
					$newPrecio->precio_mayor = $body['precioMayor'];
					$newPrecio->precio_menor = $body['precioMenor'];
					if (array_key_exists("distribuidor", $body)) {
						$newPrecio->distribuidor = $body["distribuidor"];
					}
					if (array_key_exists("especial", $body)) {
						$newPrecio->especial = $body["especial"];
					}

					$precioSaved = $newPrecio->save();

					if (!$precioSaved) {
						return response()->json([
							"success" => false,
							"content" => "Error al crear precios para Articulo ".$idArticle
						], $this->serverErr);
					} else {
						$findMoneda = Moneda::where('id', $body['idMoneda'])
							->first();

						if (!$findMoneda) {
							return response()->json([
								"success" => false,
								"content" => "Moneda ".$body['idMoneda']." no encontrada."
							], $this->notFound);
						} else {

							$newPrecioMoneda = new PrecioMoneda;
							$newPrecioMoneda->id_moneda = $findMoneda->id;
							$newPrecioMoneda->id_precio = $newPrecio->id;
							$precioMonedaSaved = $newPrecioMoneda->save();

							if (!$precioMonedaSaved) {
								return response()->json([
									"success" => true,
									"content" => "Error al asociar precio con moneda."
								], $this->serverErr);
							} else {
								return response()->json([
									"success" => true,
									"content" => "Precios asignados con exito."
								], $this->success);
							}
						}
					}
				} else {
					$findPrecio->id_articulo = $idArticle;
					$findPrecio->precio_mayor = $body['precioMayor'];
					$findPrecio->precio_menor = $body['precioMenor'];
					if (array_key_exists("distribuidor", $body)) {
						$findPrecio->distribuidor = $body["distribuidor"];
					}
					if (array_key_exists("especial", $body)) {
						$findPrecio->especial = $body["especial"];
					}
					$precioSaved = $findPrecio->save();

					if (!$precioSaved) {
						return response()->json([
							"success" => true,
							"content" => "Error al actualizar precio."
						], $this->serverErr);
					} else {
						return response()->json([
							"success" => true,
							"content" => "Precios asignados con exito."
						], $this->success);
					}
				}
			}
	}

	public function findAllArticlesAndPrices(Request $req)
	{
		$offset = ($req->offset !== null) ? $req->offset : 0;
		$limit = ($req->limit !== null) ? $req->limit : 10;
		$searchType = ($req->searchType !== null) ? $req->searchType : "all";

		$verifyArr = ['limit' => $limit, 'offset' => $offset];

		$validator = Validator::make($verifyArr, [
			'limit' => 'numeric',
			'offset' => 'numeric'
		], [
			'limit.numeric' => 'Limit debe ser numerico.',
			'offset.numeric' => 'Offset debe ser numerico.'
		]);

		if ($validator->fails()) {
			return response()->json([
				"errors" => $validator->errors()
			], $this->bad);
		}

		$user = Auth::user();
		$decodeCredentials = base64_decode($user->credentials);
		$toArr = json_decode($decodeCredentials, true);
		$cost = '';
		$costUsd = '';

		if ($toArr[0]["right"] == 0 || $toArr[0]["right"] == 1) {
			$cost = 'CONCAT("$", articulos.costo) AS COSTO,';
			$costUsd = 'articulos.costo_usd AS COSTO_USD,';

		}


		$allArticlesAndPrices = Articulo::select([DB::raw('
			articulos.id AS ID_ARTICULO,
			'.$costUsd.'
			tipos_articulos.id AS TP_ID,
			CONCAT(articulos.nombre, " | ", tipos_articulos.name, " | ", articulos.color, " | ", articulos.calidad) AS ARTICULO,
			'.$cost.'	
			CONCAT("$", precios.precio_mayor) AS PRECIO_X_MAYOR,
			CONCAT("$", precios.precio_menor) AS PRECIO_X_MENOR,
			precios.distribuidor,
			precios.especial,
			articulos.fecha_creacion,
			(SELECT cambio FROM monedas ORDER BY id DESC LIMIT 1) AS CAMBIO_DOLAR
		')])
		->leftJoin('tipos_articulos', 'tipos_articulos.id', '=', 'articulos.id_tipos_articulos')
		->leftJoin('precios', 'precios.id_articulo', '=', 'articulos.id')
		->orderBy('articulos.id', 'ASC')
        // ->take($limit)
        // ->skip($offset)
        ->get();

        return response()->json([
        	"rows" => $allArticlesAndPrices
        ], $this->success);
	}

	public function masiveImport(Request $req)
	{
		$body = $req->all();

		$parseJson = json_decode($body["prices"], true);

		for ($i = 0; $i < count($parseJson); $i++) {

			if (!is_numeric($parseJson[$i]["ID"])) {
				$parseJson[$i]["ID"] = 0;
			}

			$findPrice = Precio::where("id", $parseJson[$i]["ID"])
									->first();

			if (!$findPrice) {
				return response()->json([
					"success" => false,
					"content" => "Precio ".$parseJson[$i]["ID"]." no encontrado."
				], $this->notFound);
				break;
			} else {
				$findPrice->precio_mayor = ($parseJson[$i]["X_MAYOR"] !== "excel2json") ? $parseJson[$i]["X_MAYOR"] : 0;
				$findPrice->precio_menor = ($parseJson[$i]["X_MENOR"] !== "excel2json") ? $parseJson[$i]["X_MENOR"] : 0;

				$findPrice->save();
			}
		}

		return response()->json([
			"success" => true,
			"content" => "Precios procesados con exito."
		], $this->success);
	}
}

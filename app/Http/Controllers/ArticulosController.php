<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

// Models
use App\Articulo;
use App\TipoArticulo;

// Libs
use Validator;
use DB;

class ArticulosController extends Controller
{
	private $success = 200;
	private $bad = 400;
	private $notFound = 404;
	private $notAuthorized = 401;
	private $conflic = 409;
	private $serverErr = 500;

	public function __construct()
	{
		//
	}

	public function addArticle(Request $req)
	{
		$validator = Validator::make($req->all(), [
			"tipoArticulo" => "required",
			"nombre" => "required",
			"cantidad" => "required|integer",
			"costo" => "required|numeric",
			"costo_usd" => "required|numeric",
			"sello" => "required",
			"descripcion" => "required",
			"foto_articulo" => "sometimes|image|mimes:jpg,jpeg,bmp,png",
			"color" => "required",
			"barcode" => "required",
			"calidad" => "required",
			"origen" => "required"
		], [
			"tipoArticulo.required" => "Seleccione Tipo de Articulo para continuar.",
			"nombre.required" => "Nombre requerido",
			"cantidad.required" => "Cantidad de Items requerida.",
			"cantidad.integer" => "Cantidad de Items invalida.",
			"costo.required" => "Costo de Items requerido.",
			"costo_usd.numeric" => "Costo USD de Items invalido.",
			"costo_usd.required" => "Costo USD de Items requerido.",
			"costo.numeric" => "Costo de Items invalido.",
			"sello.required" => "Sello de Proveedor requerido.",
			"descripcion.required" => "Descripción requerida.",
			"foto_articulo.mimes" => "Formato de la Foto invalido.",
			"color.required" => "Color de Articulo requerido.",
			"barcode.required" => "Código de Barras requerido.",
			"calidad.required" => "Calidad de Articulo requerida",
			"origen.required" => "Origen de Articulo requerido"
		]);

		if ($validator->fails()) {
			return response()->json([
				"errors" => $validator->errors()
			], $this->bad);
		}

		$body = $req->all();

		$articleExist = Articulo::where('barcode', $body['barcode'])
			->first();

		if (!$articleExist) {

			$findTipoArticulo = TipoArticulo::where('name', $body['tipoArticulo'])
				->first();


			$newArticle = new Articulo;
			$newArticle->id_tipos_articulos = $findTipoArticulo->id;
			$newArticle->nombre = $body['nombre'];
			$newArticle->cantidad = $body['cantidad'];
			$newArticle->costo = $body['costo'];
			$newArticle->costo_usd = $body['costo_usd'];
			$newArticle->sello = $body['sello'];
			$newArticle->descripcion = $body['descripcion'];
			
			$newArticle->color = $body['color'];
			$newArticle->barcode = $body['barcode'];
			$newArticle->calidad = $body['calidad'];
			$newArticle->origen = $body['origen'];

			if ($file = $req->hasFile('foto_articulo')) {
				$file = $req->file('foto_articulo');
				$fileName = $file->getClientOriginalName();
				$destinationPath = public_path().'/img/articles/';
				$file->move($destinationPath, $fileName);
				$newArticle->foto_articulo = public_path().'/img/articles/'.$fileName;	
			} else {
				$newArticle->foto_articulo = public_path().'/img/articles/image-not-found.png';
			}

			$articleSaved = $newArticle->save();

			if (!$articleSaved) {
				return response()->json([
					"success" => false,
					"content" => "Error al salvar Articulo."
				], $this->serverErr);
			} else {
				return response()->json([
					"success" => true,
					"content" => "Articulo registrado con exito."
				], $this->success);
			}
		} else {
			$articleExist->cantidad = $articleExist->cantidad + 1;

			$articleExistAument = $articleExist->save();

			if (!$articleExistAument) {
				return response()->json([
					"success" => false,
					"content" => "Error al aumentar cantidad para Articulo ".$body['barcode']
				], $this->serverErr);
			} else {
				return response()->json([
					"success" => true,
					"content" => "Cantidad para el Articulo ".$body['barcode']. " aumentada con exito."
				], $this->success);
			}
		}
	}

	public function verifyBarcode(Request $req)
	{
		$validator = Validator::make($req->all(), [
			"barcode" => "required"
		], [
			"barcode.required" => "Código de Barras requerido."
		]);

		if ($validator->fails()) {
			return response()->json([
				"errors" => $validator->errors()
			], $this->bad);
		}

		$body = $req->all();

		$barcodeExist = Articulo::select([DB::raw("
			articulos.id,
			articulos.id_tipos_articulos,
			tipos_articulos.name,
			articulos.cantidad,
			articulos.calidad,
			articulos.nombre,
			articulos.color,
			articulos.costo,
			articulos.costo_usd,
			articulos.sello,
			articulos.barcode,
			articulos.origen,
			articulos.descripcion,
			articulos.calidad,
			(SELECT cambio FROM monedas ORDER BY id DESC LIMIT 1) AS CAMBIO_DOLAR
			")])
		->leftJoin('tipos_articulos', 'tipos_articulos.id', '=', 'articulos.id_tipos_articulos')
		->where('barcode', $body['barcode'])
		->first();


		if (!$barcodeExist) {
			return response()->json([
				"success" => false,
				"content" => ""
			], $this->success);
		} else {
			return response()->json([
				"success" => true,
				"content" => $barcodeExist
			],$this->success);
		}
	}

	public function findAllArticulos(Request $req)
	{
		$offset = ($req->offset !== null) ? $req->offset : 0;
        $limit = ($req->limit !== null) ? $req->limit : 10;
        $searchType = ($req->searchType !== null) ? $req->searchType : "all";

        $verifyArr = ['limit' => $limit, 'offset' => $offset];

        $validator = Validator::make($verifyArr, [
        	'limit' => 'numeric',
        	'offset' => 'numeric'
        ], [
        	'limit.numeric' => 'Limit debe ser numerico.',
        	'offset.numeric' => 'Offset debe ser numerico.'
        ]);

        if ($validator->fails()) {
        	return response()->json([
        		"errors" => $validator->errors()
        	], $this->bad);
        }

        $allArticulosFinded = Articulo::select([DB::raw("
        	articulos.id,
        	articulos.id_tipos_articulos,
        	tipos_articulos.name,
        	articulos.cantidad,
        	articulos.costo,
        	articulos.costo_usd,
        	articulos.nombre,
        	articulos.color,
        	articulos.origen,
        	articulos.descripcion,
        	articulos.sello,
        	articulos.barcode,
        	articulos.calidad,
        	(SELECT cambio FROM monedas ORDER BY id DESC LIMIT 1) AS CAMBIO_DOLAR
        ")])
        ->leftJoin('tipos_articulos', 'tipos_articulos.id', '=', 'articulos.id_tipos_articulos')
        ->orderBy('id', 'ASC')
        // ->take($limit)
        // ->skip($offset)
        ->get();

        return response()->json([
        	"rows" => $allArticulosFinded
        ], $this->success);

	}

	public function serveImage(Request $req)
	{
		$articleId = $req->route('id');

		$findImageArticle = Articulo::where('id', $articleId)	
			->first();

		$img = file_get_contents($findImageArticle->foto_articulo);
		return response($img)->header('Content-type','image/png');
	}

	public function deleteArticle(Request $req)
	{
		$idArticle = $req->route('id');

		$findArticle = Articulo::where('id', $idArticle)
			->first();

		if (!$findArticle) {
			return response()->json([
				"success" => false,
				"content" => "Articulo ".$idArticle." no encontrado."
			], $this->notFound);
		} else {
			$findArticle->delete();

			return response()->json([
				"success" => true,
				"content" => "Articulo eliminado exitosamente."
			], $this->success);
		}
	}

	public function editArticle(Request $req)
	{
		$idArticle = $req->route('id');

		$validator = Validator::make($req->all(),[
			"tipoArticulo" => "required",
			"nombre" => "required",
			"cantidad" => "required|integer",
			"costo" => "required|numeric",
			"costo_usd" => "required|numeric",
			"sello" => "required",
			"descripcion" => "required",
			"foto_articulo" => "sometimes|image|mimes:jpg,jpeg,bmp,png",
			"color" => "required",
			"barcode" => "required",
			"calidad" => "required",
			"origen" => "required"
		], [
			"tipoArticulo.required" => "Tipo de Artículo requerido",
			"nombre.required" => "Nombre de Artículo requerido.",
			"cantidad.required" => "Cantidad requerida.",
			"cantidad.integer" => "Cantidad de Items invalida.",
			"costo.required" => "Costo requerido.",
			"costo.numeric" => "Costo de Items invalido.",
			"costo_usd.required" => "Costo USD requerido.",
			"costo_usd.numeric" => "Costo USD de Items invalido.",
			"sello.required" => "Sello requerido.",
			"descripcion.required" => "Descripción requerida.",
			"foto_articulo.mimes" => "Formato de la Foto invalido.",
			"color.required" => "Color requerido.",
			"barcode.required" => "Código de Barras requerido.",
			"calidad.required" => "Calidad requerida.",
			"origen.required" => "Origen requerido."
		]);

		if ($validator->fails()) {
			return response()->json([
				"errors" => $validator->errors()
			], $this->bad);
		}

		$findArticle = Articulo::where('id', $idArticle)
			->first();

		if (!$findArticle) {
			return response()->json([
				"success" => false,
				"content" => "Articulo ".$idArticle." no encontrado."
			], $this->notFound);
		} else {
			$body = $req->all();

			if (array_key_exists('tipoArticulo', $body)) {
				$findTipoArticulo = TipoArticulo::where('name', $body['tipoArticulo'])
					->first();

				$findArticle->id_tipos_articulos = $findTipoArticulo->id;

			}
			if (array_key_exists('nombre', $body)) {
				$findArticle->nombre = $body['nombre'];

			}
			if (array_key_exists('cantidad', $body)) {
				$findArticle->cantidad = $body['cantidad'];

			}
			if (array_key_exists('costo', $body)) {
				$findArticle->costo = $body['costo'];

			}
			if (array_key_exists('costo_usd', $body)) {
				$findArticle->costo_usd = $body['costo_usd'];

			}
			if (array_key_exists('sello', $body)) {
				$findArticle->sello = $body['sello'];

			}
			if (array_key_exists('descripcion', $body)) {
				$findArticle->descripcion = $body['descripcion'];

			}
			if (array_key_exists('foto_articulo', $body)) {
				if ($file = $req->hasFile('foto_articulo')) {
					$file = $req->file('foto_articulo');
					$fileName = $file->getClientOriginalName();
					$destinationPath = public_path().'/img/articles/';
					$file->move($destinationPath, $fileName);
					$findArticle->foto_articulo = public_path().'/img/articles/'.$fileName;
				}

			}
			if (array_key_exists('color', $body)) {
				$findArticle->color = $body['color'];

			}
			if (array_key_exists('barcode', $body)) {
				$findArticle->barcode = $body['barcode'];

			}
			if (array_key_exists('calidad', $body)) {
				$findArticle->calidad = $body['calidad'];

			}
			if (array_key_exists('origen', $body)) {
				$findArticle->origen = $body['origen'];

			}

			$findArticleSaved = $findArticle->save();
			if (!$findArticleSaved) {
				return response()->json([
					"success" => false,
					"content" => "Error al actualizar articulo ".$idArticle
				], $this->serverErr);
			} else {
				return response()->json([
					"success" => true,
					"content" => "Articulo Actualizado exitosamente."
				], $this->success);
			}
		}
	}

	public function masiveImport(Request $req)
	{
		$body = $req->all();

		$parseJson = json_decode($body["prices"], true);

		for ($i = 0; $i < count($parseJson); $i++) {

			if (!is_numeric($parseJson[$i]["ID"])) {
				$parseJson[$i]["ID"] = 0;
			}

			$findArticle = Articulo::where("id", $parseJson[$i]["ID"])
									->first();

			if (!$findArticle) {
				$article = new Articulo;

				$article->barcode = ($parseJson[$i]["BARCODE"] !== "excel2json") ? $parseJson[$i]["BARCODE"] : "";
				$article->nombre = ($parseJson[$i]["NOMBRE"] !== "excel2json") ? $parseJson[$i]["NOMBRE"] : "";
				$article->costo = ($parseJson[$i]["COSTO"] !== "excel2json") ? $parseJson[$i]["COSTO"] : 0;
				$article->id_tipos_articulos = ($parseJson[$i]["TIPO_ART"] !== "excel2json") ? $parseJson[$i]["TIPO_ART"] : 7;
				$article->color = ($parseJson[$i]["COLOR"] !== "excel2json") ? $parseJson[$i]["COLOR"] : "Por definir";
				$article->calidad = ($parseJson[$i]["CALIDAD"] !== "excel2json") ? $parseJson[$i]["CALIDAD"] : "Por definir";
				$article->cantidad = ($parseJson[$i]["CANTIDAD"] !== "excel2json") ? $parseJson[$i]["CANTIDAD"] : 0;

				$article->save();
			} else {
				$findArticle->barcode = ($parseJson[$i]["BARCODE"] !== "excel2json") ? $parseJson[$i]["BARCODE"] : "";
				$findArticle->nombre = ($parseJson[$i]["NOMBRE"] !== "excel2json") ? $parseJson[$i]["NOMBRE"] : "";
				$findArticle->costo = ($parseJson[$i]["COSTO"] !== "excel2json") ? $parseJson[$i]["COSTO"] : 0;
				$findArticle->id_tipos_articulos = ($parseJson[$i]["TIPO_ART"] !== "excel2json") ? $parseJson[$i]["TIPO_ART"] : 7;
				$findArticle->color = ($parseJson[$i]["COLOR"] !== "excel2json") ? $parseJson[$i]["COLOR"] : "Por definir";
				$findArticle->calidad = ($parseJson[$i]["CALIDAD"] !== "excel2json") ? $parseJson[$i]["CALIDAD"] : "Por definir";
				$findArticle->cantidad = ($parseJson[$i]["CANTIDAD"] !== "excel2json") ? $parseJson[$i]["CANTIDAD"] : 0;

				$findArticle->save();
			}
		}

		return response()->json([
			"success" => true,
			"content" => "Articulos procesados con exito."
		], $this->success);
	}

}

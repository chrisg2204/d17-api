<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

// Libs
use Validator;
use DB;

// Models
use App\Proveedor;
use App\ProveedorSello;
use App\Sello;

class ProveedoresController extends Controller
{
	private $success = 200;
	private $bad = 400;
	private $notFound = 404;
	private $notAuthorized = 401;
	private $conflic = 409;
	private $serverErr = 500;

	public function __construct()
	{
		// 
	}

	public function addProvider(Request $req)
	{
		$validator = Validator::make($req->all(),
			[
				"id_tipo_documento" => "required|numeric",
				"n_documento" => "required|numeric",
				"nombres" => "required|regex:/^[\pL\s\-]+$/u",
				"email" => "required|email",
				"email_opt" => "nullable|email",
				"codigo_postal" => "nullable",
				"direccion" => "required",
				"telefono" => "required|numeric",
				"telefono_opt" => "nullable|numeric",
				"observacion" => "required"
			], [
				"id_tipo_documento.required" => "Tipo de Documento requerido.",
				"id_tipo_documento.numeric" => "Tipo de Documento inválido.",
				"n_documento.required" => "Número de Documento requerido.",
				"n_documento.numeric" => "Número de Documento inválido.",
				"nombres.required" => "Nombres requerido.",
				"nombres.regex" => "Nombre no debe tener caracteres especiales.",
				"email.required" => "Email requerido",
				"email.email" => "Formato de email principal inválido",
				"email_opt.email" => "Formato de email opcional inválido",
				"direccion.required" => "Dirección requerida.",
				"telefono.required" => "Teléfono principal requerido.",
				"telefono.numeric" => "Teléfono principal debe ser numerico.",
				"telefono_opt.numeric" => "Teléfono opcional debe ser numerico",
				"observacion.required" => "Observación requerida."
			]);

		if ($validator->fails()) {
			return response()->json([
				"errors" => $validator->errors()
			], $this->bad);
		}

		$body = $req->all();

		$findOneNDocument = Proveedor::where("n_documento", $body["n_documento"])
										->first();

		$findOneEmail = Proveedor::where("email", $body["email"])
									->first();

		$findOnePhone = Proveedor::where("telefono", $body["telefono"])
									->first();
		if ($findOneNDocument) {
			return response()->json([
				"success" => false,
				"content" => "Número de Documento ".$body["n_documento"]." ya existe."
			], $this->bad);
		}
		if ($findOneEmail) {
			return response()->json([
				"success" => false,
				"content" => "Email ".$body["email"]." ya existe."
			], $this->bad);
		}
		if ($findOnePhone) {
			return response()->json([
				"success" => false,
				"content" => "Teléfono ".$body["telefono"]." ya existe"
			], $this->bad);
		}
		if ($body["email"] == $body["email_opt"]) {
			return response()->json([
				"success" => false,
				"content" => "Email principal no debe ser igual a email opcional."
			], $this->conflic);
		}
		if ($body["email_opt"] == $body["email"]) {
			return response()->json([
				"success" => false,
				"content" => "Email opcional no debe ser igual a email principal."
			],$this->conflic);
		}
		if ($body["telefono"] == $body["telefono_opt"]) {
			return response()->json([
				"success" => false,
				"content" => "Teléfono principal no debe ser igual a teléfono opcional."
			], $this->conflic);
		}
		if ($body["telefono_opt"] == $body["telefono"]) {
			return response()->json([
				"success" => false,
				"content" => "Teléfono opcional no debe ser igual a teléfono principal."
			], $this->conflic);
		}
		if ($body["id_tipo_documento"] == 1 || $body["id_tipo_documento"] == 2) {
			if (strlen($body["n_documento"]) < 11) {
				return response()->json([
					"success" => false,
					"content" => "Número de Documento ".$body["n_documento"]." invalido."
				], $this->bad);	
			}
		} elseif ($body["id_tipo_documento"] == 3) {
			if (strlen($body["n_documento"]) <> 8) {
				return response()->json([
					"success" => false,
					"content" => "Número de Documento ".$body["n_documento"]." invalido."
				], $this->bad);
			}
		}

		$newProvider = new Proveedor;
		$newProvider->id_tipo_documento = $body["id_tipo_documento"];
		$newProvider->n_documento = $body["n_documento"];
		$newProvider->nombres = $body["nombres"];
		$newProvider->email = $body["email"];
		if (array_key_exists("email_opt", $body)) {
			$newProvider->email_opt = $body["email_opt"];
		}
		if (array_key_exists("codigo_postal", $body)) {
			$newProvider->codigo_postal = $body["codigo_postal"];
		}
		$newProvider->direccion = $body["direccion"];
		$newProvider->telefono = $body["telefono"];
		if (array_key_exists("telefono_opt", $body)) {
			$newProvider->telefono_opt = $body["telefono_opt"];
		}
		$newProvider->observacion = $body["observacion"];
		$newProvider->estatus = 1;

		$providerSaved = $newProvider->save();

		if (!$providerSaved) {
			return response()->json([
				"success" => false,
				"content" => "Error al registrar Proveedor."
			],$this->serverErr);
		} else {
			return response()->json([
				"success" => true,
				"content" => "Proveedor registrado exitosamente."
			], $this->success);
		}
	}

	public function addSeal(Request $req)
	{
		$idProvider = $req->route("id");
		$validator = Validator::make($req->all(),
			[
				"descripcion" => "required",
				"fotoSello" => "required|image|mimes:jpg,jpeg,bmp,png"
			], [
				"descripcion.required" => "Descripción requerida.",
				"fotoSello.required" => "Foto del sello requerida.",
				"fotoSello.image" => "Foto del sello invalida.",
				"fotoSello.mimes" => "Formato de Foto del sello invalido."
			]);

		if ($validator->fails()) {
			return response()->json([
				"errors" => $validator->errors()
			], $this->bad);
		}

		$body = $req->all();

		$newSeal = new Sello;
		$newSeal->descripcion = $body["descripcion"];

		if ($file = $req->hasFile('fotoSello')) {
			$file = $req->file('fotoSello');
			$fileName = $file->getClientOriginalName();
			$destinationPath = public_path().'/img/seal/';
			$file->move($destinationPath, $fileName);
			$newSeal->path_image = public_path().'/img/seal/'.$fileName;
		} else {
			$newSeal->path_image = public_path().'/img/seal/image-not-found.png';
		}

		$sealSaved = $newSeal->save();
		if (!$sealSaved) {

			return response()->json([
				"success" => false,
				"content" => "Error al agregar Foto del Sello a Proveedor ".$idProvider
			], $this->serverErr);
		} else {
			$providerSeal = new ProveedorSello;
			$providerSeal->id_proveedor = $idProvider;
			$providerSeal->id_sello = $newSeal->id;

			$providerSealSaved = $providerSeal->save();

			if (!$providerSealSaved) {
				return response()->json([
					"success" => false,
					"content" => "Error al asociar Sello ".$providerSeal->id." con Proveedor ".$idProvider
				], $this->serverErr);
			} else {
				return response()->json([
					"success" => true,
					"content" => "Sello agregado exitosamente."
				], $this->success);
			}
		}
	}

	public function findSeal(Request $req)
	{
		$idProvider = $req->route("id");

		$findAllSealProvider = Proveedor::select([DB::raw(
			"
				sellos.id AS SEAL_ID,
				sellos.descripcion AS SEAL_DES
			")])
		->rightJoin("proveedor_sello", "proveedor_sello.id_proveedor", "=", "proveedores.id")
		->rightJoin("sellos", "sellos.id", "=", "proveedor_sello.id_sello")
		->where("proveedores.id", $idProvider)
		->get();

		return response()->json([
			"rows" => $findAllSealProvider
		], $this->success);

		// if (!$findAllSealProvider) {
		// 	return response()->json([
		// 		"success" => false,
		// 		"content" => "Error al buscar sellos por proveedor"
		// 	], $this->serverErr);
		// } else {
		// 	return response()->json([
		// 		"success" => true,
		// 		"content" => $findAllSealProvider
		// 	], $this->success);
		// }
	}

	public function serveSeal(Request $req)
	{
		$idSeal = $req->route("id");

		$findSealImage = Sello::where("id", $idSeal)
								->first();

		$img = file_get_contents($findSealImage->path_image);

		return response($img)->header('Content-type','image/png');
	}

	public function editProvider(Request $req)
	{
		$idProvider = $req->route("id");

		$validator = Validator::make($req->all(),
			[
				"id_tipo_documento" => "nullable|numeric",
				"n_documento" => "nullable|numeric",
				"nombres" => "nullable|regex:/^[\pL\s\-]+$/u",
				"email" => "nullable|email",
				"email_opt" => "nullable|email",
				"codigo_postal" => "nullable",
				"direccion" => "nullable",
				"telefono" => "nullable|numeric",
				"telefono_opt" => "nullable|numeric",
				"observacion" => "nullable"
			], [
				"id_tipo_documento.numeric" => "Tipo de Documento inválido.",
				"n_documento.numeric" => "Número de Documento inválido.",
				"nombres.regex" => "Nombre no debe tener caracteres especiales.",
				"email.email" => "Formato de email principal inválido.",
				"email_opt.email" => "Formato de email opcional inválido.",
				"telefono.numeric" => "Teléfono principal debe ser numerico.",
				"telefono_opt.numeric" => "Teléfono opcional debe ser numerico."
			]);

		if ($validator->fails()) {
			return response()->json([
				"errors" => $validator->errors()
			], $this->bad);
		}

		$body = $req->all();

		$findOneProvider = Proveedor::where("id", $idProvider)
										->first();

		if (!$findOneProvider) {
			return response()->json([
				"success" => false,
				"content" => "Proveedor ".$idProvider." no encontrado."
			], $this->notFound);
		} else {
			if (array_key_exists("id_tipo_documento", $body)) {
				$findOneProvider->id_tipo_documento = $body["id_tipo_documento"];
			}
			if (array_key_exists("n_documento", $body)) {
				$findOneProvider->n_documento = $body["n_documento"];
			}
			if (array_key_exists("nombres", $body)) {
				$findOneProvider->nombres = $body["nombres"];
			}
			if (array_key_exists("email", $body)) {
				$findOneProvider->email = $body["email"];
			}
			if (array_key_exists("email_opt", $body)) {
				$findOneProvider->email_opt = $body["email_opt"];
			}
			if (array_key_exists("codigo_postal", $body)) {
				$findOneProvider->codigo_postal = $body["email_opt"];
			}
			if (array_key_exists("direccion", $body)) {
				$findOneProvider->direccion = $body["direccion"];
			}
			if (array_key_exists("telefono", $body)) {
				$findOneProvider->telefono = $body["telefono"];
			}
			if (array_key_exists("telefono_opt", $body)) {
				$findOneProvider->telefono_opt = $body["telefono_opt"];
			}
			if (array_key_exists("observacion", $body)) {
				$findOneProvider->observacion = $body["observacion"];
			}

			$providerSaved = $findOneProvider->save();

			if (!$providerSaved) {
				return response()->json([
					"success" => false,
					"content" => "Error al actualizar datos del Proveedor ".$idProvider
				], $this->bad);
			} else {
				return response()->json([
					"success" => true,
					"content" => "Proveedor actualizado exitosamente."
				], $this->success);
			}
		}
	}

	public function softDeleteProvider(Request $req)
	{
		$idProvider = $req->route("id");

		$findOneProvider = Proveedor::where("id", $idProvider)
										->first();

		if (!$findOneProvider) {
			return response()->json([
				"success" => false,
				"content" => "Proveedor ".$idProvider." no encontrado."
			], $this->notFound);
		} else {
			$findOneProvider->estatus = 0;

			$providerSaved = $findOneProvider->save();

			if (!$providerSaved) {
				return response()->json([
					"success" => false,
					"content" => "Error al inhabilitado Proveedor."
				], serverErr);
			} else {
				return response()->json([
					"success" => true,
					"content" => "Proveedor inhabilitado exitosamente."
				], $this->success);
			}
		}
	}

	public function deleteSeal(Request $req)
	{
		$idSeal = $req->route("id");

		$findOneSeal = Sello::where("id", $idSeal)
								->first();

		if (!$findOneSeal) {
			return response()->json([
				"success" => false,
				"content" => "Sello ".$idSeal." no encontrado"
			], $this->notFound);
		}

		$findOneSeal->delete();

		return response()->json([
			"success" => true,
			"content" => "Sello eliminado exitosamente."
		], $this->success);
	}

	public function findAllProvider(Request $req)
	{
		$offset = ($req->offset !== null) ? $req->offset : 0;
		$limit = ($req->limit !== null) ? $req->limit : 10;
		$searchType = ($req->searchType !== null) ? $req->searchType : "all";

		$verifyArr = ['limit' => $limit, 'offset' => $offset];

		$validator = Validator::make($verifyArr, [
			'limit' => 'numeric',
			'offset' => 'numeric'
		], [
			'limit.numeric' => 'Limit debe ser numerico.',
			'offset.numeric' => 'Offset debe ser numerico.'
		]);

		if ($validator->fails()) {
			return response()->json([
				"errors" => $validator->errors()
			], $this->bad);
		}

		$allProvider = Proveedor::select([DB::raw(
			"
				proveedores.id AS ID,
				proveedores.id_tipo_documento AS ID_TIP_DOC,
				tipos_documentos.descripcion AS TIP_DOC_DES,
				proveedores.n_documento AS N_DOC,
				proveedores.email AS EMAIL,
				proveedores.email_opt AS EMAIL_OPT,
				proveedores.nombres AS NOM,
				proveedores.direccion AS DIR,
				proveedores.codigo_postal AS COD_POS,
				proveedores.telefono AS TEL,
				proveedores.telefono_opt AS TEL_OPT,
				proveedores.observacion AS OBS,
				proveedores.estatus AS EST,
				proveedores.fecha_creacion AS FEC_CRE
			"
		)])
		->leftjoin('tipos_documentos', 'tipos_documentos.id', '=', 'proveedores.id_tipo_documento')
		->where('estatus', 1)
		->orderBy('proveedores.id', 'DESC')
		->get();

		return response()->json([
			"rows" => $allProvider
		], $this->success);
	}

	public function findAllDisabledProvider(Request $req)
	{
		$offset = ($req->offset !== null) ? $req->offset : 0;
		$limit = ($req->limit !== null) ? $req->limit : 10;
		$searchType = ($req->searchType !== null) ? $req->searchType : "all";

		$verifyArr = ['limit' => $limit, 'offset' => $offset];

		$validator = Validator::make($verifyArr, [
			'limit' => 'numeric',
			'offset' => 'numeric'
		], [
			'limit.numeric' => 'Limit debe ser numerico.',
			'offset.numeric' => 'Offset debe ser numerico.'
		]);

		if ($validator->fails()) {
			return response()->json([
				"errors" => $validator->errors()
			], $this->bad);
		}

		$disabledProviders = Proveedor::select([DB::raw(
			"
				proveedores.id AS ID,
				proveedores.id_tipo_documento AS ID_TIP_DOC,
				tipos_documentos.descripcion AS TIP_DOC_DES,
				proveedores.n_documento AS N_DOC,
				proveedores.email AS EMAIL,
				proveedores.email_opt AS EMAIL_OPT,
				proveedores.nombres AS NOM,
				proveedores.direccion AS DIR,
				proveedores.codigo_postal AS COD_POS,
				proveedores.telefono AS TEL,
				proveedores.telefono_opt AS TEL_OPT,
				proveedores.observacion AS OBS,
				proveedores.estatus AS EST,
				proveedores.fecha_creacion AS FEC_CRE
			"
		)])
		->leftjoin("tipos_documentos", "tipos_documentos.id", "=", "proveedores.id_tipo_documento")
		->where("estatus", 0)
		->orderBy("proveedores.id", "DESC")
		->get();

		return response()->json([
			"rows" => $disabledProviders
		], $this->success);

	}

	public function enableProvider(Request $req)
	{
		$idProvider = $req->route("id");

		$findOneProvider = Proveedor::where("id", $idProvider)
										->first();

		if (!$findOneProvider) {
			return response()->json([
				"success" => false,
				"content" => "Proveedor ".$idProvider." no encontrado."
			], $this->notFound);
		}

		$findOneProvider->estatus = 1;

		$providerSaved = $findOneProvider->save();

		if (!$providerSaved) {
			return response()->json([
				"success" => false,
				"content" => "Error al habilitar Proveedor."
			], $this->serverErr);
		} else {
			return response()->json([
				"success" => true,
				"content" => "Proveedor habilitado exitosamente."
			], $this->success);
		}
	}

}

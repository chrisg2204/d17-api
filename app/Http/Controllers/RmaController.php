<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

// Libs
use Validator;
use Uuid;
use DB;

// Models
use App\Articulo;
use App\Venta;
use App\Cliente;
use App\FacturaCliente;
use App\CtaCte;
use App\CtaCteDetalleCtaCte;
use App\CtaCteDetalle;
use App\Devolucion;

class RmaController extends Controller
{
	private $success = 200;
    private $bad = 400;
    private $notFound = 404;
    private $notAuthorized = 401;
    private $conflic = 409;
    private $serverErr = 500;

    public function __construct()
    {
        // 
    }

    public function getSalesArticles(Request $req)
    {
    	$clientId = $req->route("id");

    	$validator = Validator::make($req->all(),
    		[
    			"interval" => "required|integer",
    			"barcode" => "required"
    		], [
    			"interval.required" => "Intervalos de Días requerido.",
    			"interval.integer" => "Intervalos deben ser Enteros.",
    			"barcode.required" => "Código de Barras requerido.",
    		]);


    	if ($validator->fails()) {
    		return response()->json([
    			"errors" => $validator->errors()
    		], $this->bad);
    	}

    	$body = $req->all();
    	$artArr = [];

    	$getClient = Cliente::where("id", $clientId)
    						->first();
    	if (!$getClient) {
    		return response()->json([
    			"success" => false,
    			"content" => "Cliente ".$clientId." no encontrado."
    		], $this->notFound);
    	}

    	$getLastSales = FacturaCliente::select([DB::raw(
    		'
    			items_articulo AS SAL_RAN
    		'
    	)])
    	->leftJoin("facturas", "factura_cliente.id_factura", "=", "facturas.id")
    	->leftJoin("ventas", "facturas.id_venta", "=", "ventas.id")
    	->where("factura_cliente.id_client", $getClient->id)
    	->whereRaw("ventas.fecha_creacion BETWEEN DATE_SUB(NOW(), INTERVAL ".$body["interval"]." DAY) AND NOW()")
    	->get();

    	if (count($getLastSales)) {
    		$getArticleId = Articulo::where("barcode", $body["barcode"])
    								->first();

    		if (!$getArticleId) {
    			return response()->json([
    				"success" => false,
    				"content" => "Articulo ".$body["barcode"]." no encontrado."
    			], $this->notFound);
    		}

    		$lastSalesArr = json_decode($getLastSales, true);


    		for ($i = 0; $i < count($lastSalesArr); $i++) {
    			if ($getArticleId->id == json_decode($lastSalesArr[$i]["SAL_RAN"], true)[0]["ID"]) {
    				array_push($artArr, json_decode($lastSalesArr[$i]["SAL_RAN"], true)[0]);

    			}
    		}

    		return response()->json([
    			"rows" => $artArr
    		], $this->success);
    	} else {
    		return response()->json([
    			"rows" => []
    		], $this->success);
    	}
    }

    public function refund(Request $req)
    {
        $clientId = $req->route("id");

        $validator = Validator::make($req->all(),
            [
                "cantidad" => "integer|required",
                "items" => "string|required",
                "observacion" => "required",
                "tipo" => "integer|required"
            ], [
                "cantidad.required" => "Cantidad requerida.",
                "cantidad.integer" => "Cantidad debe ser un Integer.",
                "items.string" => "Items deb ser un String.",
                "items.required" => "Items requerido.",
                "observacion.required" => "Observación requerida.",
                "tipo.integer" => "Tipo debe ser un Integer.",
                "tipo.required" => "Tipo requerido."
            ]);

        if ($validator->fails()) {
            return response()->json([
                "errors" => $validator->errors()
            ], $this->bad);
        }

        $body = $req->all();

        $devolution = new Devolucion;

        $devolution->articulos = $body["items"];
        $devolution->id_cliente = $clientId;
        $devolution->status = 1;
        $devolution->observacion = $body["observacion"];
        $devolution->tipo = $body["tipo"];

        $devolutionSaved = $devolution->save();

        if (!$devolutionSaved) {
            return response()->json([
                "success" => false,
                "content" => "Error al solicitar devolucion."
            ], $this->serverErr);
        } else {
            return response()->json([
                "success" => true,
                "content" => "Devolucion solicitada exitosamente."
            ], $this->success);
        }
    }
}

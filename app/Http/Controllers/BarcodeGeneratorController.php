<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class BarcodeGeneratorController extends Controller
{
	private $success = 200;
	private $bad = 400;
	private $notFound = 404;
	private $notAuthorized = 401;
	private $conflic = 409;
	private $serverErr = 500;

	public function __construct()
	{
		//
	}

	public function barcode(Request $req)
	{
		$barcode = $req->route('code');
		return view('barcodegenerator', ['code' => $barcode]);
	}
}

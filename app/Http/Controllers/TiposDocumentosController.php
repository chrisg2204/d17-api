<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

// Libs
use Validator;
use DB;

// Models
use App\TipoDocumento;

class TiposDocumentosController extends Controller
{
	private $success = 200;
	private $bad = 400;
	private $notFound = 404;
	private $notAuthorized = 401;
	private $confict = 409;
	private $serverErr = 500;

	public function __construct()
	{
		//
	}

	public function addTipoDocumento(Request $req)
	{
		$validator = Validator::make($req->all(), [
			"descripcion" => "required"
		],[
			"descripcion.required" => "Descripcion requerida."
		]);

		if ($validator->fails()) {
			return response()->json([
				"error" => $validator->errors()
			], $this->bad);
		}

		$body = $req->all();

		$tipoDocumentoExist = TipoDocumento::where('descripcion', $body['descripcion'])
			->first();

		if (!$tipoDocumentoExist) {
			$newTipoDocumento = new TipoDocumento;
			$newTipoDocumento->descripcion = $body["descripcion"];

			$tipoDocumentoSaved = $newTipoDocumento->save();

			if (!$tipoDocumentoSaved) {
				return response()->json([
					"success" => false,
					"content" => "Error al registrar Tipo de Documento."
				], $this->serverErr);
			} else {
				return response()->json([
					"success" => true,
					"content" => "Tipo de Documento registrado con exito."
				], $this->success);
			}
		} else {
			return response()->json([
				"success" => false,
				"content" => "Tipo de Documento ya existe."
			], $this->confict);
		}
	}

	public function deleteTipoDocumento(Request $req)
	{
		$pathParam = $req->route('idTipoDocumento');

		$tipoDocumentoExist = TipoDocumento::where('id', $pathParam)
			->first();

		if (!$tipoDocumentoExist) {
			return response()->json([
				"success" => false,
				"content" => "Tipo de documento no encontrado."
			], $this->notFound);
		} else {
			$tipoDocumentoExist->delete();

			return response()->json([
				"success" => true,
				"content" => "Tipo de documento eliminado con exito."
			], $this->success);
		}
	}

	public function fillTipoDocumento(Request $req)
	{
		$findAllTipoDocumento = TipoDocumento::select([DB::raw(
			"id AS ID,
			descripcion AS DESCRIPTION"
		)])
		->get();

		return response()->json([
			"rows" => $findAllTipoDocumento
		], $this->success);
	}
}

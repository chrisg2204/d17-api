<?php

namespace App\Http\Controllers;


use Illuminate\Http\Request;

// Libs
use App\Exports\UsersExport;
use App\Exports\ArticlesExport;
use App\Exports\PrecioExport;
use Maatwebsite\Excel\Facades\Excel;
use App\Http\Controllers\Controller;

// Models
use App\Articulo;


class ExportImportController extends Controller
{
	private $success = 200;
	private $bad = 400;
	private $notFound = 404;
	private $notAuthorized = 401;
	private $conflic = 409;
	private $serverErr = 500;

	public function __construct()
	{
		//
	}

	public function exportXLSXArticle(Request $req)
	{
		return Excel::download(new ArticlesExport, 'ListaArticulos.xlsx');
	}

	public function exportXLSXPrecio(Request $req)
	{
		return Excel::download(new PrecioExport, 'ListaPrecios.xlsx');
	}

}

<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

// Libs
use Validator;
use Uuid;
use DB;

// Models
use App\Articulo;
use App\TipoArticulo;
use App\Venta;
use App\Factura;
use App\FacturaCliente;
use App\Cliente;

class VentasController extends Controller
{
	private $success = 200;
	private $bad = 400;
	private $notFound = 404;
	private $notAuthorized = 401;
	private $conflic = 409;
	private $serverErr = 500;

	public function __construct()
	{
		// 
	}

	public function verifyBarcodeForSales(Request $req)
	{
		$validator = Validator::make($req->all(), [
			"barcode" => "required"
		], [
			"barcode.required" => "Código de Barras requerido."
		]);

		if ($validator->fails()) {
			return response()->json([
				"errors" => $validator->errors()
			], $this->bad);
		}

		$body = $req->all();

		$findOneBarcode = Articulo::select([DB::raw(
			'
				articulos.id AS AID,
				articulos.barcode AS BARCODE,
	   			CONCAT(articulos.nombre, " | ", tipos_articulos.name, " | ", articulos.color, " | ", articulos.calidad) AS ARTICULO,
       			articulos.cantidad AS CANT,
       			precios.precio_mayor AS PRECIO_X_MAYOR,
       			precios.precio_menor AS PRECIO_X_MENOR
			'
		)])
		->join("tipos_articulos", "articulos.id_tipos_articulos", "=", "tipos_articulos.id")
		->join("precios", "articulos.id", "=", "precios.id_articulo")
		->orderBy('articulos.id', 'ASC')
		->where('barcode', $body['barcode'])
		->first();

		if ($findOneBarcode == null) {
			return response()->json([
				"success" => false,
				"content" => "Artículo no tiene precio estipulado."
			], $this->notFound);
		} else {
			return response()->json([
				"success" => true,
				"content" => $findOneBarcode
			], $this->success);
		}
	}

	public function consolidateSale(Request $req)
	{
		$validator = Validator::make($req->all(),
			[
				"tipoVenta" => "required",
				"items_venta" => "required",
				"total" => "required|numeric"
			], [
				"tipoVenta.required" => "Tipo de Venta requerido.",
				"total.required" => "Resultado total requerido",
				"total.numeric" => "Resultado total invalido.",
				"items_venta.required" => "Items de la Venta requeridos."
			]);


		if ($validator->fails()) {
			return response()->json([
				"errors" => $validator->errors()
			], $this->bad);
		}

		$body = $req->all();
		$verifyItems = json_decode($body["items_venta"], true);

		for ($h = 0; $h < count($verifyItems); $h++) {

			$verify = Articulo::where("id", $verifyItems[$h]["ID"])->first();

			if ($verifyItems[$h]["CANTIDAD"] > $verify->cantidad) {
				return response()->json([
					"success" => false,
					"content" => "Solo hay ".$verify->cantidad." del Articulo ".$verify->nombre
				], $this->conflic);
			}

		}

		$sale = new Venta;
		$sale->uuid = Uuid::generate(4);
		$sale->tipo_venta = $body["tipoVenta"];
		$sale->items_articulo = $body["items_venta"];
		$sale->total = $body["total"];

		$saleSaved = $sale->save();

		if (!$saleSaved) {
			return response()->json([
				"success" => false,
				"content" => "Error al consolidar la venta."
			], $this->serverErr);
		} else {
			if ($body["tipoVenta"] == 1) {
				$factura = new Factura;
				$factura->id_venta = $sale->id;
				$factura->id_seller = Auth::user()->id;
				$factura->correlativo_factura = $this->getSequece();
				$factura->status = 0;

				$facturaSaved = $factura->save();

				if (!$facturaSaved) {
					return response()->json([
						"success" => false,
						"content" => "Error al crear factura."
					], $this->success);
				} else {
					$itemsToArr = json_decode($body["items_venta"], true);

					for ($i = 0; $i < count($itemsToArr); $i++) {
						$findArt = Articulo::where("id", $itemsToArr[$i]["ID"])->first();
						$findArt->cantidad = $findArt->cantidad - $itemsToArr[$i]["CANTIDAD"];
						$findArt->save();

					}

					return response()->json([
						"success" => true,
						"content" => "Venta consolidada exitosamente."
					],$this->success);
				}
			} else if ($body["tipoVenta"] == 2) {
				$factura = new Factura;
				$factura->id_venta = $sale->id;
				$factura->id_seller = Auth::user()->id;
				$factura->correlativo_factura = $this->getSequece();
				$factura->status = 0;

				$facturaSaved = $factura->save();

				if (!$facturaSaved) {
					return response()->json([
						"success" => false,
						"content" => "Error al crear factura."
					], $this->success);
				} else {
					$itemsToArr = json_decode($body["items_venta"], true);

					for ($i = 0; $i < count($itemsToArr); $i++) {
						$findArt = Articulo::where("id", $itemsToArr[$i]["ID"])->first();
						$findArt->cantidad = $findArt->cantidad - $itemsToArr[$i]["CANTIDAD"];
						$findArt->save();

					}

					$factClient = new FacturaCliente;
					$factClient->id_factura = $factura->id;
					$factClient->id_client = $body["idClient"];

					$factClientSaved = $factClient->save();

					if (!$factClientSaved) {
						return response()->json([
							"success" => false,
							"content" => "Error al asociar Factura ".$factura->id." con Cliente ".$body["idClient"]
						], $this->serverErr);
					} else {
						return response()->json([
							"success" => true,
							"content" => "Venta consolidada exitosamente."
						],$this->success);
					}
				}
			} else {
				return false;
			}
		}
	}

	public function getSequece()
	{
		$sqlGetSequence = DB::select("
			SELECT LAST_INSERT_ID() AS LAST_ID;		
		");

		$array = json_decode(json_encode($sqlGetSequence), true);

		$sequence = ($array[0]["LAST_ID"] == 0) ? $array[0]["LAST_ID"] + 1 : $array[0]["LAST_ID"];

		return "F-".$sequence;
	}

}

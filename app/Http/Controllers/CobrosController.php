<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

// Libs
use Validator;
use Uuid;
use DB;

// Models
use App\Cliente;
use App\Banco;
use App\ChequeBanco;
use App\Cobros;
use App\Cheque;
use App\CobroCheque;
use App\CobrosFactura;
use App\Venta;
use App\Factura;
use App\CtaCte;
use App\ClienteCtaCte;
use App\CtaCteDetalleCtaCte;
use App\CtaCteDetalle;
use App\User;



class CobrosController extends Controller
{
	private $success = 200;
	private $bad = 400;
	private $notFound = 404;
	private $notAuthorized = 401;
	private $conflic = 409;
	private $serverErr = 500;

	public function __construct()
	{
		// 
	}

	public function listSales(Request $req)
	{
		$findAllSales = Venta::select([DB::raw(
			"
				ventas.id AS ID,
				ventas.tipo_venta AS TIP_VEN,
				ventas.items_articulo AS ITEMS,
				ventas.total AS TTL,
				ventas.fecha_creacion AS CRE_FEC,
				facturas.id AS ID_FAC,
				facturas.status AS EST,
				facturas.correlativo_factura AS COR_FAC,
				clientes.id AS ID_CLI,
				clientes.nombres AS NOM,
				clientes.direccion AS DIR
			"
		)])
		->join("facturas", "ventas.id", "=", "facturas.id_venta")
		->leftJoin("factura_cliente", "facturas.id", "=", "factura_cliente.id_factura")
		->leftJoin("clientes", "factura_cliente.id_client", "=", "clientes.id")
		->where("facturas.status", 0)
		->orderBy("ventas.id", "DESC")
		->get();

		return response()->json([
			"rows" => $findAllSales
		], $this->success);
	}

	public function getFavor(Request $req)
	{
		$idBuyer = $req->route("id");

		$getBalanceFavor = ClienteCtaCte::select([DB::raw(
			"
				(cuenta_cte.saldo * -1) AS SAL_FAV
			"
		)])
		->join("cuenta_cte", "cliente_cta_cte.id_cta_cte", "=", "cuenta_cte.id")
		->where("cliente_cta_cte.id_cliente", $idBuyer)
		->orderBy("cuenta_cte.id", "DESC")
		->limit(1)
		->get();

		return response()->json([
			"success" => true,
			"content" => $getBalanceFavor
		], $this->success);

	}

	public function getAllBanks(Request $req)
	{
		$findAllBanks = Banco::select([DB::raw(
			"
				bancos.id AS id,
				bancos.codigo AS code,
				bancos.nombre AS name
			"
		)])
		->get();

		return response()->json([
			"rows" => $findAllBanks
		], $this->success);
	}

	public function consolidateCharge(Request $req)
	{
		$validator = Validator::make($req->all(),
			[
				"idClient" => "required|numeric",
				"idSale" => "required|numeric",
				"idFac" => "required|numeric",
				"inFavor" => "required|numeric",
				"typeSale" => "required|numeric",
				"giroDesc" => "nullable",
				"purchaseItems" => "required|string",
				"checkTotal" => "nullable",
				"paymentTotal" => "required|string",
				"totalRef" => "required|numeric"
			], [
				"idClient.required" => "Id del Cliente Requerido.",
				"idClient.numeric" => "Id del Cliente debe ser Numérico.",
				"idSale.required" => "Id de la Venta Requerido.",
				"idSale.numeric" => "Id de la Venta debe ser Numérico.",
				"idFac.required" => "Id de la Factura Requerido.",
				"idFac.numeric" => "Id de la Factura debe ser Numérico.",
				"inFavor.required" => "Saldo a Favor Requerido.",
				"inFavor.numeric" => "Saldo a Favor debe ser Numérico.",
				"typeSale.required" => "Tipo de Venta Requerido.",
				"typeSale.numeric" => "Tipo de Venta debe ser Numérico.",
				"purchaseItems.required" => "Items de la Compra Requerido.",
				"purchaseItems.string" => "Items de la Compra debe ser String.",
				"paymentTotal.required" => "Total de Pagos Requerido.",
				"paymentTotal.string" => "Total de Pagos debe ser String.",
				"totalRef.required" => "Total de Pago Referencial Requerido.",
				"totalRef.numeric" => "Total de Pago Referencial debe ser Numérico",
			]);

		if ($validator->fails()) {
			return response()->json([
				"errors" => $validator->errors()
			], $this->bad);
		}

		$body = $req->all();
		$chargeUser = Auth::user()->id;
		$sumItemsTotal = 0;
		$sumTotales = 0;

		$parsePurchaseItems = json_decode($body["purchaseItems"], true);
		$parsePaymentTotal = json_decode($body["paymentTotal"], true);
		$parseCheckTotal = ($body["checkTotal"] != null) ? json_decode($body["checkTotal"], true) : [];

		for ($i = 0; $i < count($parsePurchaseItems); $i++) {
			$sumItemsTotal = ($sumItemsTotal + $parsePurchaseItems[$i]["subtotal"]);
		}

		$sumTotales = ($sumTotales + $parsePaymentTotal["cash"] + $parsePaymentTotal["debit"] + $parsePaymentTotal["check"]);

		if ($sumItemsTotal !== $body["totalRef"]) {
			return response()->json([
				"success" => false,
				"message" => "Suma de Articulos es diferente a el Total Referenciado."
			], $this->conflic);
		}

		$findOneFact = Factura::where("id", $body["idFac"])
								->first();

		if ($findOneFact->status == 1) {
			return response()->json([
				"success" => false,
				"content" => "Factura ".$body["idFac"]." ya ha sido procesada."
			], $this->conflic);

			// TODO
			// Guardar registro de usuario 
		} else {
			if (Auth::check()) {
				
				$charge = new Cobros;
				$charge->id_pay_user = Auth::user()->id;
				$charge->type_payment = $body["paymentTotal"];
				$charge->type_sell = $body["typeSale"];

				$chargeSaved = $charge->save();

				if (!$chargeSaved) {
					return response()->json([
						"success" => false,
						"content" => "Error al Cobrar Venta."
					], $this->serverErr);
				} else {
					$chargeFac = new CobrosFactura;
					$chargeFac->id_factura = $findOneFact->id;
					$chargeFac->id_cobro = $charge->id;

					$chargeFacSaved = $chargeFac->save();

					if (!$chargeFacSaved) {
						return response()->jso([
							"success" => false,
							"content" => "Error al asociar Factura ".$findOneFact->id." con Cobro ".$charge->id
						], $this->serverErr);
					} else {
						$findOneFact->status = 1;
						$findOneFact->save();

						if ($body["typeSale"] == 2) {
							if (count($parseCheckTotal)) {
								for ($j = 0; $j < count($parseCheckTotal); $j++) {
									$check = new Cheque;
									$bankCheck = new ChequeBanco;

									$check->uuid = Uuid::generate(4);
									$check->numero = $parseCheckTotal[$j]["num"];
									$check->monto = $parseCheckTotal[$j]["mont"];
									$check->propietario = $parseCheckTotal[$j]["owner"];
									$check->titular = $parseCheckTotal[$j]["creator"];
									$check->fecha_emision = $parseCheckTotal[$j]["inDate"];
									$check->fecha_cobro = $parseCheckTotal[$j]["outDate"];

									$check->save();

									$bankCheck->cheque_id = $check->id;
									$bankCheck->banco_id = $parseCheckTotal[$j]["bankId"];

									$bankCheck->save();
								}
							}

							$ctaCteLastSaldo = CtaCte::select([DB::raw(
								"
									cuenta_cte.saldo AS SAL
								"
							)])
							->join("cliente_cta_cte", "cuenta_cte.id", "=", "cliente_cta_cte.id_cta_cte")
							->where("cliente_cta_cte.id_cliente", $body["idClient"])
							->orderBy("cliente_cta_cte.id", "DESC")
							->get();

							$arrResult = (count(json_decode(json_encode($ctaCteLastSaldo), true)) == 0) ? 0 : json_decode(json_encode($ctaCteLastSaldo), true);

							$ctaCte = new CtaCte;
							$cliCtaCte = new ClienteCtaCte;
							$ctaCteDet = new CtaCteDetalle;
							$ctaCteDetCtaCte = new CtaCteDetalleCtaCte;

							$ctaCte->descripcion_giro = ($body["giroDesc"] != null) ? $body["giroDesc"] : "Compra por Mayor | Local.";
							$ctaCte->deuda_cliente = ($parsePaymentTotal["ctaCte"] != 0) ? $parsePaymentTotal["ctaCte"] : 0;
							$ctaCte->monto_giro = ($body["inFavor"] != 0) ? $body["inFavor"] : 0;
							$ctaCte->saldo = (count($arrResult == 1)) ? $arrResult[0]["SAL"] + $parsePaymentTotal["ctaCte"] - $body["inFavor"] : 0;

							$ctaCte->save();

							$cliCtaCte->id_cliente = $body["idClient"];
							$cliCtaCte->id_cta_cte = $ctaCte->id;

							$cliCtaCte->save();

							$ctaCteDet->uuid = Uuid::generate(4);
							$ctaCteDet->items_detalle = $body["purchaseItems"];
							$ctaCteDet->total = $sumItemsTotal;
							$ctaCteDet->n_factura = $findOneFact->correlativo_factura;

							$ctaCteDet->save();

							$ctaCteDetCtaCte->id_cta_cte = $ctaCte->id;
							$ctaCteDetCtaCte->id_detalle_cte_cta = $ctaCteDet->id;

							$ctaCteDetCtaCte->save();
						}

						$req->user()->token()->delete();
						$user = User::where("id", $chargeUser)->first();

						$token = $user->createToken('MyApp')->accessToken;

						return response()->json([
							"success" => true,
							"content" => [
								"message" => "Cobro realizado Exitosamente.",
								"token" => $token
							]
						], $this->success);
					}
				}
			} else {
				return response()->json([
					"success" => true,
					"content" => "Sesión invalida. Inicie sesión para continuar."
				], $this->notFound);
			}
		}
	}

	public function cosolidatePayment(Request $req)
	{
		$validator = Validator::make($req->all(),
			[
				"idClient" => "required|numeric",
				"idSale" => "required|numeric",
				"idFac" => "required|numeric",
				"inFavor" => "required|numeric",
				"typeSale" => "required|numeric",
				"giroDesc" => "nullable",
				"purchaseItems" => "required|string",
				"checkTotal" => "nullable",
				"paymentTotal" => "required|string",
				"totalRef" => "required|numeric"
			], [
				"idClient.required" => "Id del Cliente Requerido.",
				"idClient.numeric" => "Id del Cliente debe ser Numérico.",
				"idSale.required" => "Id de la Venta Requerido.",
				"idSale.numeric" => "Id de la Venta debe ser Numérico.",
				"idFac.required" => "Id de la Factura Requerido.",
				"idFac.numeric" => "Id de la Factura debe ser Numérico.",
				"inFavor.required" => "Saldo a Favor Requerido.",
				"inFavor.numeric" => "Saldo a Favor debe ser Numérico.",
				"typeSale.required" => "Tipo de Venta Requerido.",
				"typeSale.numeric" => "Tipo de Venta debe ser Numérico.",
				"purchaseItems.required" => "Items de la Compra Requerido.",
				"purchaseItems.string" => "Items de la Compra debe ser String.",
				"paymentTotal.required" => "Total de Pagos Requerido.",
				"paymentTotal.string" => "Total de Pagos debe ser String.",
				"totalRef.required" => "Total de Pago Referencial Requerido.",
				"totalRef.numeric" => "Total de Pago Referencial debe ser Numérico",
			]);

		if ($validator->fails()) {
			return response()->json([
				"errors" => $validator->errors()
			], $this->bad);
		}

		$body = $req->all();
		$chargeUser = Auth::user()->id;
		$sumItemsTotal = 0;
		$sumTotales = 0;

		$parsePurchaseItems = json_decode($body["purchaseItems"], true);
		$parsePaymentTotal = json_decode($body["paymentTotal"], true);

		for ($i = 0; $i < count($parsePurchaseItems); $i++) {
			$sumItemsTotal = ($sumItemsTotal + $parsePurchaseItems[$i]["subtotal"]);
		}

		if ($sumItemsTotal !== $body["totalRef"]) {
			return response()->json([
				"success" => false,
				"message" => "Suma de Articulos es diferente a el Total Referenciado."
			], $this->conflic);
		}

		$findOneFact = Factura::where("id", $body["idFac"])
								->first();

		if ($findOneFact->status == 1) {
			return response()->json([
				"success" => false,
				"content" => "Factura ".$body["idFac"]." ya ha sido procesada."
			], $this->conflic);

			// TODO
			// Guardar registro de usuario 
		} else {
			if (Auth::check()) {
				
				$charge = new Cobros;
				$charge->id_pay_user = Auth::user()->id;
				$charge->type_payment = $body["paymentTotal"];
				$charge->type_sell = $body["typeSale"];

				$chargeSaved = $charge->save();

				if (!$chargeSaved) {
					return response()->json([
						"success" => false,
						"content" => "Error al Cobrar Venta."
					], $this->serverErr);
				} else {
					$chargeFac = new CobrosFactura;
					$chargeFac->id_factura = $findOneFact->id;
					$chargeFac->id_cobro = $charge->id;

					$chargeFacSaved = $chargeFac->save();

					if (!$chargeFacSaved) {
						return response()->jso([
							"success" => false,
							"content" => "Error al asociar Factura ".$findOneFact->id." con Cobro ".$charge->id
						], $this->serverErr);
					} else {
						$findOneFact->status = 1;
						$findOneFact->save();

						$ctaCteLastSaldo = CtaCte::select([DB::raw(
							"
								cuenta_cte.saldo AS SAL
							"
						)])
						->join("cliente_cta_cte", "cuenta_cte.id", "=", "cliente_cta_cte.id_cta_cte")
						->where("cliente_cta_cte.id_cliente", $body["idClient"])
						->orderBy("cliente_cta_cte.id", "DESC")
						->get();

						// $arrResult = json_decode(json_encode($ctaCteLastSaldo), true);

						$arrResult = (count(json_decode(json_encode($ctaCteLastSaldo), true)) == 0) ? 0 : json_decode(json_encode($ctaCteLastSaldo), true);

						// print_r($arrResult);

						// $arrResult[0]["SAL"]
						// TODO

						$ctaCte = new CtaCte;
						$cliCtaCte = new ClienteCtaCte;
						$ctaCteDet = new CtaCteDetalle;
						$ctaCteDetCtaCte = new CtaCteDetalleCtaCte;

						$ctaCte->descripcion_giro = ($body["giroDesc"] != null) ? $body["giroDesc"] : "Compra por Mayor | Local.";
						$ctaCte->deuda_cliente = ($parsePaymentTotal["ctaCte"] != 0) ? $parsePaymentTotal["ctaCte"] : 0;
						$ctaCte->monto_giro = 0.00;
						$ctaCte->saldo = (count($arrResult) == 1) ? ($arrResult[0]["SAL"] + $parsePaymentTotal["ctaCte"]) : $arrResult[0]["SAL"] + $parsePaymentTotal["ctaCte"];

						$ctaCte->save();

						$cliCtaCte->id_cliente = $body["idClient"];
						$cliCtaCte->id_cta_cte = $ctaCte->id;

						$cliCtaCte->save();

						$ctaCteDet->uuid = Uuid::generate(4);
						$ctaCteDet->items_detalle = $body["purchaseItems"];
						$ctaCteDet->total = $sumItemsTotal;
						$ctaCteDet->n_factura = $findOneFact->correlativo_factura;

						$ctaCteDet->save();

						$ctaCteDetCtaCte->id_cta_cte = $ctaCte->id;
						$ctaCteDetCtaCte->id_detalle_cte_cta = $ctaCteDet->id;

						$ctaCteDetCtaCte->save();

						$req->user()->token()->delete();
						$user = User::where("id", $chargeUser)->first();
						$token = $user->createToken('MyApp')->accessToken;

						return response()->json([
							"success" => true,
							"content" => [
								"message" => "Cobro realizado Exitosamente.",
								"token" => $token
							]
						], $this->success);
					}
				}
			} else {
				return response()->json([
					"success" => true,
					"content" => "Sesión invalida. Inicie sesión para continuar."
				], $this->notFound);
			}
		}
	}
}

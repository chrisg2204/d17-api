<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Validator;
use DB;
use App\TipoArticulo;

class TiposArticulosController extends Controller
{
	private $success = 200;
	private $bad = 400;
	private $notFound = 404;
	private $notAuthorized = 401;
	private $conflic = 409;
	private $serverErr = 500;

	public function __construct()
	{
		//
	}

	/**
	 * [add description]
	 * @param Request $req [description]
	 */
	public function add(Request $req) {
		$validator = Validator::make($req->all(), [
			'nombre' => 'required|regex:/^[\pL\s\-]+$/u',
		], [
			'nombre.required' => 'Nombre requerido',
			'nombre.regex' => 'Solo letras permitidas para Nombre'
		]);

		if ($validator->fails()) {
			return response()->json(['error' => $validator->errors()], 400);
		}

		$body = $req->all();
		$tipoArticuloFinded = TipoArticulo::where("name", "=", $body["nombre"])->first();
		if ($tipoArticuloFinded === null) {
			$tiposArticulos = new TipoArticulo;
			$tiposArticulos->name = $body["nombre"];
			$tiposArticulosSaved = $tiposArticulos->save();

			if (!$tiposArticulosSaved) {
				return response()->json(["success" => false, "content" => "Error al registrar Tipo de Articulo."], 500);
			} else {
				return response()->json(["success" => true, "content" => "Tipo de Articulo registrado."], 200);
			}
		} else {
			return response()->json(["success" => false, "content" => "Tipo de Articulo ya existe."], 409);
		}
	}

	/**
	 * [findAll description]
	 * @param  Request $req [description]
	 * @return [type]       [description]
	 */
	public function findAll(Request $req) {
		$searchType = ($req->searchType !== null) ? $req->searchType : "all";

		$allTipoArticulo = TipoArticulo::select([DB::raw(
			"id,
			name,
			fecha_creacion"
		)])
		->orWhere("name", "like", "%".$req->search."%")
		->orderBy("id", $req->order)
		->get();

		return response()->json(["rows" => $allTipoArticulo], 200);
	}

	public function fillSelectTiposArticulos(Request $req) {
		$getTiposArticulos = TipoArticulo::select([DB::raw(
			"id,
			name"
		)])
		->get();

		return response()->json([
			"rows" => $getTiposArticulos
		], $this->success);
	}


}

<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

// Libs.
use Validator;
use Mail;
use DB;

// Models
use App\User;


class PassportController extends Controller
{
	private $success = 200;
	private $bad = 400;
	private $notFound = 404;
	private $notAuthorized = 401;
	private $servErr = 500;

	public function __construct() {
		// 
	}

	public function login(Request $req) {
		$validator = Validator::make($req->all(), [
			'email' => 'required|email',
			'password' => 'required'
		], [
			'email.required' => 'El email debe ser obligatorio.',
			'email.email' => 'Formato de email inválido.',
			'password.required' => 'La contraseña debe ser obligatoria.'
		]);

		if ($validator->fails()) {
			return response()->json([
				'error' => $validator->errors()
			], $this->bad);
		}

		$body = $req->all();

		if (Auth::attempt([
			'email' => $body['email'],
			'password' => $body['password']
		])) {
			$userAuth = Auth::user();
			$token = $userAuth->createToken('MyApp')->accessToken;

			return response()->json([
				'user' => $userAuth,
				'token' => $token
			], $this->success);
		} else {

			return response()->json([
				'success' => false,
				'content' => 'Usuario o contraseña invalidos.'
			], $this->notAuthorized);
		}
	}

	/**
	 * [logout description]
	 * @param  Request $req [description]
	 * @return [type]       [description]
	 */
	public function logout(Request $req) {
		if (Auth::check()) {
			$req->user()->token()->delete();

			return response()->json(['success' => true, 'content' => 'Sesión finalizada.'], $this->success);
		}
	}

	public function register(Request $req) {

		$validator = Validator::make($req->all(), [
			'nombres' => 'required|regex:/^[\pL\s\-]+$/u',
			'apellido' => 'required|regex:/^[\pL\s\-]+$/u',
			'privilegio' => 'required|numeric',
			'email' => 'required|email',
			'password' => 'required',
			'c_password' => 'required|same:password'
		], [
			'nombres.required' => 'El nombre debe ser obligatorio.',
			'nombres.regex' => 'El nombre no debe tener caracteres especiales.',
			'apellido.required' => 'El apellido debe ser obligatorio.',
			'apellido.regex' => 'El apellido no debe tener caracteres especiales.',
			'privilegio.required' => 'Seleccione un Privilegio.',
			'privilegio.numeric' => 'Privilegio invalido.',
			'email.required' => 'El email debe ser obligatorio.',
			'email.email' => 'Formato de email inválido.',
			'password.required' => 'La contraseña debe ser obligatoria.',
			'c_password.required' => 'Confirma contraseña para continuar.',
			'c_password.same' => 'Contraseñas no coinciden.'
		]);

		if ($validator->fails()) {
			return response()->json([
				'error' => $validator->errors()
			], $this->bad);            
		}

		$body = $req->all();

		$userExist = User::where('email', $body['email'])
							->first();

		if (!$userExist) {
			$user = new User;
			$credential = [];

			$user->email = $body["email"];
			$user->nombres = $body["nombres"];
			$user->apellido = $body["apellido"];
			$user->password = bcrypt($body['password']);
			
			$success['token'] =  $user->createToken('MyApp')->accessToken;
        	$success['name'] =  $user->nombres;

        	$userSaved = $user->save();

        	if (!$userSaved) {
        		return response()->json([
        			"success" => false,
        			"content" => "Error al registar Usuario ".$body["email"]
        		], $this->$servErr);
        	} else {
        		if ($body["privilegio"] == 1) {
        			array_push($credential, ["right" => 1]);
        			array_push($credential, ["motive" => "black"]);
        			array_push($credential, ["barcode" => "GRAL ".$this->getSequece()]);
        			array_push($credential, ["modules" => 
        				[
        					["title" => "Perfil", "icon" => "account_circle"],
        					["title" => "Home", "icon" => "dashboard"],
        					["title" => "Usuario", "icon" => "person_add"],
        					["title" => "Artículos", "icon" => "shopping_cart"],
        					["title" => "Precios", "icon" => "insert_chart"],
        					["title" => "Cotización", "icon" => "attach_money"],
        					["title" => "Clientes", "icon" => "person_add"],
        					["title" => "Proveedor", "icon" => "local_shipping"],
        					["title" => "CTA/CTE CLI", "icon" => "bar_chart"],
        					["title" => "CTA/CTE PROV", "icon" => "bar_chart"],
        					["title" => "Ventas", "icon" => "add_shopping_cart"],
        					["title" => "Facturas", "icon" => "note"],
        					["title" => "Cobros", "icon" => "payment"],
        					["title" => "Reparto", "icon" => "directions_car"],
        					["title" => "Libro Diario", "icon" => "bookmarks"],
        					["title" => "RMA", "icon" => "table_chart"]
        				]
        			]);


				} else if ($body["privilegio"] == 2) {
					array_push($credential, ["right" => 2]);
					array_push($credential,  ["motive" => "teal lighten-1"]);
					array_push($credential, ["barcode" => "CAJR ".$this->getSequece()]);
					array_push($credential, ["modules" => 
        				[
        					["title" => "Perfil", "icon" => "account_circle"],
        					["title" => "Home", "icon" => "dashboard"],
        					["title" => "Artículos", "icon" => "shopping_cart"],
        					["title" => "Precios", "icon" => "insert_chart"],
        					["title" => "Cobros", "icon" => "payment"]
        				]
        			]);

				} else if ($body["privilegio"] == 3) {
					array_push($credential, ["right" => 3]);
					array_push($credential,  ["motive" => "pink lighten-1"]);
					array_push($credential, ["barcode" => "VEND ".$this->getSequece()]);
					array_push($credential, ["modules" => 
						[
							["title" => "Perfil", "icon" => "account_circle"],
							["title" => "Home", "icon" => "dashboard"],
							["title" => "Artículos", "icon" => "shopping_cart"],
							["title" => "Clientes", "icon" => "person_add"],
							["title" => "Ventas", "icon" => "add_shopping_cart"],
							["title" => "Precios", "icon" => "insert_chart"],
							["title" => "Reparto", "icon" => "directions_car"]
						]
					]);

				} else if ($body["privilegio"] == 4) {
					array_push($credential, ["right" => 4]);
					array_push($credential,  ["motive" => "blue-grey lighten-1"]);
					array_push($credential, ["barcode" => "REPR ".$this->getSequece()]);
					array_push($credential, ["modules" => 
						[
							["title" => "Perfil", "icon" => "account_circle"],
							["title" => "Home", "icon" => "dashboard"],
							["title" => "Reparto", "icon" => "directions_car"]
						]
					]);

				}  else {
					array_push($credential, ["right" => 0]);
					array_push($credential,  ["motive" => "indigo lighten-1"]);
					array_push($credential, ["barcode" => "ROOT ".$this->getSequece()]);
					array_push($credential, ["modules" => 
						[
							["title" => "Perfil", "icon" => "account_circle"],
							["title" => "Home", "icon" => "dashboard"],
							["title" => "Usuario", "icon" => "person_add"],
							["title" => "Artículos", "icon" => "shopping_cart"],
							["title" => "Precios", "icon" => "insert_chart"],
							["title" => "Cotización", "icon" => "attach_money"],
							["title" => "Clientes", "icon" => "person_add"],
							["title" => "Proveedor", "icon" => "local_shipping"],
							["title" => "CTA/CTE CLI", "icon" => "bar_chart"],
							["title" => "CTA/CTE PROV", "icon" => "bar_chart"],
							["title" => "Ventas", "icon" => "add_shopping_cart"],
							["title" => "Facturas", "icon" => "note"],
							["title" => "Cobros", "icon" => "payment"],
        					["title" => "Reparto", "icon" => "directions_car"],
        					["title" => "Libro Diario", "icon" => "bookmarks"],
        					["title" => "RMA", "icon" => "table_chart"]
						]
					]);
				}

				$user->credentials = base64_encode(json_encode($credential));
				$user->save();

        		return response()->json([
        			"success" => true,
        			"content" => "Registro exitoso."
        		], $this->success);
        	}	
        } else {
        	return response()->json([
        		"success" => false,
        		"content" => "Email ".$body["email"]." ya existe."
        	], $this->bad);
        }
	}

	/**
	 * [getDetail description]
	 * @return [type] [description]
	 */
	public function getDetail() {
		if (Auth::check()) {
			$user = Auth::user();

			return response()->json(['success' => true, 'content' => $user], $this->success);
		}
	}

	/**
	 * [changePassword description]
	 * @param  Request $req [description]
	 * @return [type]       [description]
	 */
	public function changePassword(Request $req) {
		if (Auth::check()) {
			$validator = Validator::make($req->all(), [
				'n_password' => 'required',
				'c_password' => 'required|same:n_password'
			], [
				'n_password.required' => 'La contraseña debe ser obligatoria.',
				'c_password.required' => 'Confirma contraseña para continuar.',
				'c_password.same' => 'Contraseñas no coinciden.'
			]);

			if ($validator->fails()) {

				return response()->json(['error' => $validator->errors()], $this->bad);
			}

			$body = $req->all();
			$user = Auth::user();

			$user->password = bcrypt($body['n_password']);

			$use->save();
			$user->token()->revoke();
			$user->createToken('MyApp')->accessToken;

			return response()->json(['success' => true, 'content' => 'Contraseña cambiada exitosamente'], $this->success);
		}
	}

	public function getUsers(Request $req)
	{
		$offset = ($req->offset !== null) ? $req->offset : 0;
        $limit = ($req->limit !== null) ? $req->limit : 10;
        $searchType = ($req->searchType !== null) ? $req->searchType : "all";

        $verifyArr = ['limit' => $limit, 'offset' => $offset];

        $validator = Validator::make($verifyArr, [
        	'limit' => 'numeric',
        	'offset' => 'numeric'
        ], [
        	'limit.numeric' => 'Limit debe ser numerico.',
        	'offset.numeric' => 'Offset debe ser numerico.'
        ]);

        if ($validator->fails()) {
        	return response()->json([
        		"errors" => $validator->errors()
        	], $this->bad);
        }

        $findAllUsers = User::select([DB::raw(
        	'
        		users.id AS ID,
    			users.email AS EMA,
    			users.nombres AS NOM,
    			users.apellido AS APE,
    			CONCAT(users.nombres, " ", users.apellido) AS FUL_NOM,
    			users.status AS STA,
    			users.credentials AS CRE,
    			users.fecha_creacion AS FEC_CRE
        	'
        )])
        ->where("users.status", 1)
        ->where("users.id", "!=", 1)
        ->orderBy("users.id", "ASC")
        ->get();

        return response()->json([
        	"rows" => $findAllUsers
        ], $this->success);
	}

	public function disableUser(Request $req)
	{
		$userId = $req->route("id");

		$findUser = User::where("id", $userId)
						->first();

		if (!$findUser) {
			return response()->json([
				"success" => false,
				"content" => "Usuario ".$userId." no encontrado."
			], $this->notFound);
		} else {
			$findUser->status = 0;

			$userUpdate = $findUser->save();

			if (!$userUpdate) {
				return response()->json([
					"success" => false,
					"content" => "Error al inhabilitar el usuario ".$userId
				], $this->servErr);
			} else {
				return response()->json([
					"success" => true,
					"content" => "Usuario inhabilitado con exito."
				], $this->success);
			}
		}
	}

	public function getSequece()
	{
		$sqlGetSequence = DB::select("
			SELECT LAST_INSERT_ID() AS LAST_ID;		
		");

		$array = json_decode(json_encode($sqlGetSequence), true);

		$sequence = ($array[0]["LAST_ID"] == 0) ? $array[0]["LAST_ID"] + 1 : $array[0]["LAST_ID"];

		return "E-".$sequence;
	}
}

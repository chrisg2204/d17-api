<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class CuentaCteProvDet extends Model
{
	protected $fillable = [
		"uuid",
		"items_detalle",
		"total",
		"n_factura",
		"fecha_movimiento"
	];

	protected $guarded = [
		"id"
	];

	protected $table = "cuenta_cte_prov_det";

	public $timestamps = false;
}

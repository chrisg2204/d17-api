<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Sello extends Model
{
	protected $fillable = [
		"descripcion",
		"path_image"
	];

	protected $guarded = [
		"id"
	];

	protected $table = "sellos";

	public $timestamps = false;
}

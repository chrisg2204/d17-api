<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Articulo extends Model
{
	protected $fillable = [
		'id_tipos_articulos',
		'nombre',
		'cantidad',
		'costo',
		'sello',
		'descripcion',
		'foto_articulo',
		'color',
		'barcode',
		'calidad',
		'origen',
		'costo_usd'
	];

	protected $guarded = [
		'id'
	];

	protected $table = 'articulos';

	public $timestamps = false;
}

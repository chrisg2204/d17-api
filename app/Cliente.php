<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Cliente extends Model
{
	protected $fillable = [
		"id_tipo_documento",
		"n_documento",
		"email",
		"email_opt",
		"nombres",
		"direccion",
		"codigo_postal",
		"telefono",
		"telefono_opt",
		"observacion",
		"estatus",
		"fecha_creacion"
	];

	protected $guarded = [
		"id"
	];

	protected $table = "clientes";

	public $timestamps = false;
}

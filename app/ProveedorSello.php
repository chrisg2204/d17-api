<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ProveedorSello extends Model
{
	protected $fillable = [
		"id_proveedor",
		"id_sello"
	];

	protected $guarded = [
		"id"
	];

	protected $table = "proveedor_sello";

	public $timestamps = false;
}

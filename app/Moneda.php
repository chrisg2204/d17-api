<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Moneda extends Model
{
	protected $fillable = [
		"denominacion",
		"simbolo",
		"cambio",
		"fecha_creacion"
	];

	protected $guarded = [
		"id"
	];

	protected $table = "monedas";

	public $timestamps = false;
}

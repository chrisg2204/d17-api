<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ProveedorCtaCte extends Model
{
	protected $fillable = [
		"uuid",
		"descripcion_giro_propio",
		"deuda_propia",
		"monto_giro_propio",
		"saldo_propio",
		"fecha_movimiento"
	];

	protected $guarded = [
		"id"
	];

	protected $table = "prov_cuenta_cte";

	public $timestamps = false;
}

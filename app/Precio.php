<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Precio extends Model
{
	protected $fillable = [
		"id_articulo",
		"precio_mayor",
		"precio_menor",
		"distribuidor",
		"especial",
		"fecha_creacion"
	];

	protected $guarded = [
		"id"
	];

	protected $table = "precios";

	public $timestamps = false;
}

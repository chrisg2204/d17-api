<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class PrecioMoneda extends Model
{
	protected $fillable = [
		"id_precio",
		"id_moneda"
	];

	protected $guarded = [
		"id"
	];

	protected $table = "precios_monedas";

	public $timestamps = false;
}

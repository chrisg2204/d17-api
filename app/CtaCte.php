<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class CtaCte extends Model
{
	protected $fillable = [
		"descripcion_giro",
		"deuda_cliente",
		"monto_giro",
		"saldo",
		"fecha_movimiento"
	];

	protected $guarded = [
		"id"
	];

	protected $table = "cuenta_cte";

	public $timestamps = false;
}

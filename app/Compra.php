<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Compra extends Model
{
	protected $fillable = [
		"id_tipos_articulos",
		"uuid",
		"nombre_articulo",
		"cantidad",
		"status",
		"costo",
		"fecha",
	];

	protected $guarded = [
		"id"
	];

	protected $table = "compras";

	public $timestamps = false;
}

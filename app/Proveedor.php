<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Proveedor extends Model
{
	protected $fillabler = [
		"id_tipo_documento",
		"n_documento",
		"nombres",
		"email",
		"email_opt",
		"codigo_postal",
		"direccion",
		"telefono",
		"telefono_opt",
		"estatus",
		"observacion"
	];

	protected $guaded = [
		"id"
	];

	protected $table = "proveedores";

	public $timestamps = false;
}

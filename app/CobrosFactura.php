<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class CobrosFactura extends Model
{
	protected $fillable = [
		"id_factura",
		"id_cobro"
	];

	protected $guarded = [
		"id"
	];

	protected $table = "cobros_factura";

	public $timestamps = false;
}

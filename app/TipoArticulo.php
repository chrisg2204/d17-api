<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class TipoArticulo extends Model
{
	protected $fillable = ["name"];

	protected $guarded = ["id"];

	protected $table = "tipos_articulos";

	public $timestamps = false;
}
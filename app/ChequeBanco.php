<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ChequeBanco extends Model
{
	protected $fillable = [
		"cheque_id",
		"banco_id"
	];

	protected $guarded = [
		"id"
	];

	protected $table = "cheque_banco";

	public $timestamps = false;
}

<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

// Inicio de sesión.
Route::post('/login', 'PassportController@login');

// Registrar usuario.
Route::post('/registrer', 'PassportController@register');


// Rutas protegidas.
Route::group(['middleware' => 'auth:api'], function() {
	// Cerrar sesión.
	Route::delete('/logout', 'PassportController@logout');
	
	/**
	 * Clientes
	 */
	
	// Agrega un cliente.
	Route::post('/cliente/add', 'ClientesController@addClient');

	// Actualiza los datos de un cliente
	Route::put('/cliente/edit/{id}', 'ClientesController@editClient');

	// Lista todos los clientes.
	Route::get('/cliente/all', 'ClientesController@findAllClients');

	// Lista todos los clientes inhabilidos.
	Route::get('/cliente/all/disable', 'ClientesController@getDisabledClient');

	// Habilita un cliente.
	Route::put('/cliente/enable/{id}', 'ClientesController@enableClient');

	// Elimina lógicamente un cliente.
	Route::delete('/cliente/soft-delete/{id}', 'ClientesController@softDeleteClient');

	// Llena select de tipos de documentos.
	Route::get('/tipos-documentos/fill', 'TiposDocumentosController@fillTipoDocumento');

	/**
	 * Tiposde Documentos
	 */
	
	// Agrega un tipo de documento
	Route::post('/tipo-documento/add', 'TiposDocumentosController@addTipoDocumento');

	// Elimina un tipo de documento
	Route::delete('/tipo-documento/del/{idTipoDocumento}', 'TiposDocumentosController@deleteTipoDocumento');

	/**
	 * Tipos de Articulos
	 */
	
	// Llena select de tipos de articulos.
	Route::get('/tipos-articulos/fill', 'TiposArticulosController@fillSelectTiposArticulos');

	// Verifica si existe un articulo por medio del código de barras.
	Route::post('/tipos-articulos/verify', 'ArticulosController@verifyBarcode');

	/**
	 * Articulos
	 */
	
	// Lista todos los articulos.
	Route::get('/articulos/all', 'ArticulosController@findAllArticulos');

	// Agrega un articulo.
	Route::post('/articulos/add', 'ArticulosController@addArticle');

	// Eliminar un articulo.
	Route::delete('/articulos/delete/{id}', 'ArticulosController@deleteArticle');

	// Agregar precios.
	Route::post('/articulos/{id}/price', 'PreciosController@addPrices');

	// Lista todos los articulos junto a sus precios.
	Route::get('/articulos/precios', 'PreciosController@findAllArticlesAndPrices');

	// Obtiene todas las cotizaciones.
	Route::get('/currency/all', 'MonedaController@findAllCurrency');

	// Acualiza la cotización del $.
	Route::post('/currency', 'MonedaController@addCurrency');

	// Llena el select de cotización.
	Route::get('/currency/fill', 'MonedaController@fillCurrency');

	/**
	 * Proveedor
	 */
	
	// Agrega proveedores.
	Route::post("/provider/add", "ProveedoresController@addProvider");

	// Agrega sellos a un proveedor.
	Route::post("/provider/{id}/seal", "ProveedoresController@addSeal");

	// Encuentra los sellos del proveedor.
	Route::get("/provider/{id}/seal", "ProveedoresController@findSeal");

	// Actualiza los datos de un proveedor.
	Route::put("/provider/edit/{id}", "ProveedoresController@editProvider");

	// Inhabilita un proveedor.
	Route::delete("/provider/soft-delete/{id}", "ProveedoresController@softDeleteProvider");

	// Elimina fisicamente un sello (foto).
	Route::delete("/provider/seal/{id}", "ProveedoresController@deleteSeal");

	// Lista todos los proveedores
	Route::get("/provider/all", "ProveedoresController@findAllProvider");

	// Lista todos los proveedores inhabilitados.
	Route::get("/provider/all/disable", "ProveedoresController@findAllDisabledProvider");

	// Habilita un Proveedor.
	Route::put("/provider/enable/{id}", "ProveedoresController@enableProvider");

	/**
	 * Cta/Cte
	 */
	
	// Llena select de clientes.
	Route::get("/client/fill", "CuentasCorrientesController@fillClientCtaCte");

	// Obtiene la cuenta corriente de un cliente.
	Route::get("/client/{id}/account", "CuentasCorrientesController@getAccount");

	// Agrega un movimiento a la cuenta corriente de un cliente.
	Route::post("/client/{id}/movement", "CuentasCorrientesController@addAccountMovement");

	// Obtiene el detalle de un movimiento.
	Route::get("/client/movement/{id}/detail/invoice/{codeInv}", "CuentasCorrientesController@getDetailMovement");

	/**
	 * Cta/Cte (Proveedor)
	 */
	
	// Llena select de proveedores.
	Route::get("/provider/fill", "ProveedoresCuentasCorrientes@fillProviderCtaCte");

	// Obtiene la cuenta corriente de un proveedor.
	Route::get("/provider/{id}/account", "ProveedoresCuentasCorrientes@getAccount");

	// Agrega un movimiento a la cuenta corriente de un proveedor.
	Route::post("/provider/{id}/movement", "ProveedoresCuentasCorrientes@addAccountMovement");
	
	// Obtiene el detalle de un movimiento.
	Route::get("/movement/{id}/detail", "ProveedoresCuentasCorrientes@getDetailMovement");

	/**
	 * Ventas
	 */
	
	// Verifica que exista el código de barra para agragar a venta.
	Route::post("/barcode-verify", "VentasController@verifyBarcodeForSales");

	// Consolida la venta.
	Route::post("/sale", "VentasController@consolidateSale");

	/**
	 * Cobros
	 */
	
	// 
	Route::get("/get-sales", "CobrosController@listSales");

	//
	Route::get("/get-banks", "CobrosController@getAllBanks");

	Route::post("/charge", "CobrosController@consolidateCharge");

	Route::post("/charge/cta-cte", "CobrosController@cosolidatePayment");

	Route::get("/get-favor/{id}", "CobrosController@getFavor");

	/**
	 * Importar - Exportar
	 */
	
	Route::put("/import/article", "ArticulosController@masiveImport");

	Route::put("/import/price", "PreciosController@masiveImport");

	/**
	 * Usuarios
	 */
	
	Route::get("/users/get", "PassportController@getUsers");

	Route::delete("/users/soft-delete/{id}", "PassportController@disableUser");

	/**
	 * RMA
	 */
	
	Route::post("/rma/client/{id}", "RmaController@getSalesArticles");
	
	Route::post("/rma/devolution/client/{id}", "RmaController@refund");

});

/**
 * Agrega tipos de articulos.
 */
Route::post('/tipos-articulos/add', 'TiposArticulosController@add');

/**
 * Lista todos los tipos de articulos.
 */
Route::get('/tipos-articulos/all', 'TiposArticulosController@findAll');

/**
 * Servir foto de articulo.
 */
Route::get('/articulos/img/{id}', 'ArticulosController@serveImage');

// Actualiza un articulo.
Route::put('/articulos/update/{id}', 'ArticulosController@editArticle');

// Genera el código de barras.
Route::get('/articulos/barcode/{code}', 'BarcodeGeneratorController@barcode');

// Sirve el sello del proveedor.
Route::get("/provider/seal/{id}/serve", "ProveedoresController@serveSeal");

/**
 * Importar - Exportar
 */

Route::get("/export/article", "ExportImportController@exportXLSXArticle");

Route::get("/export/price", "ExportImportController@exportXLSXPrecio");


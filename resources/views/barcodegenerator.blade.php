<!-- barcodegenerator.blade.php --> 

<!DOCTYPE html>
<html>
  <head>
    <meta charset="utf-8">
    <title>Barcode Generator </title>
    <link rel="stylesheet" href="{{asset('css/app.css')}}">
   </head>
<body>
<div class="container text-center">
<h2>One-Dimensional (1D) Barcode</h2><br/>
   <div>{!!DNS1D::getBarcodeSVG($code, 'C128', 1, 33)!!}</div>
   <h5>{!!$code!!}</h5>
</br>
 </div>
</body>
</html>